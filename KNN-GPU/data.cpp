#include <iostream>
using std::ofstream;
using std::ifstream;
#include <fstream>
#include <sstream>
#include <string>
using std::string;
#include <algorithm>
#include "lea.h"
#include "main.h"

template<class T>
T fromString(const std::string& s) {
     std::istringstream stream (s);
     T t;
     stream >> t;
     return t;
}

void synthetic_data_array(float *data[], int length, int width, float outlierRate, float outlierOffset, int outlierDims) {
	int outlierDimsCount;
	int outlierCount = (int)(length * outlierRate);
	if (outlierCount > length || outlierCount < 0) {
		std::cerr << "synthetic_data_array: Invalid outlier rate" << std::endl;
		std::cin.get(); // Wait before exit
		exit(EXIT_FAILURE);
	}

	for (int row = 0; row < length; row++) {
		outlierDimsCount = outlierDims;
		for (int col = 0; col < width; col++) {
			if (outlierCount > 0 && outlierDimsCount > 0) {
				(*data)[row*width+col] = (float)random_normal() + outlierOffset;
			} else {
				(*data)[row*width+col] = (float)random_normal();
			}
			outlierDimsCount--;
		}
		outlierCount--;
	}
}

void synthetic_data_array_clusters(float *data[], int length, int width, float outlierRate, float outlierOffset, int outlierDims, int outlierDistributions) {
	int outlierDimsCount;
	float meanOffset = 1.5f, stdevMultiplier = 0.5f;
	int currentDistribution = 0;
	int outlierCount = (int)(length * outlierRate);
	if (outlierCount > length || outlierCount < 0) {
		std::cerr << "synthetic_data_array_clusters: Invalid outlier rate" << std::endl;
		std::cin.get(); // Wait before exit
		exit(EXIT_FAILURE);
	} else if (outlierDistributions < 1) {
		std::cerr << "synthetic_data_array_clusters: Invalid outlier distributions" << std::endl;
		std::cin.get(); // Wait before exit
		exit(EXIT_FAILURE);
	}

	for (int row = 0; row < length; row++) {
		outlierDimsCount = outlierDims;
		for (int col = 0; col < width; col++) {
			if (outlierCount > 0 && outlierDimsCount > 0) {
				(*data)[row*width+col] = (float)random_normal() * outlierOffset; // Outliers are not clustered
				outlierDimsCount--;
			} else {
				(*data)[row*width+col] = (float)(random_normal() * stdevMultiplier) + (meanOffset*currentDistribution);
				currentDistribution = (currentDistribution++) % outlierDistributions; // outlierDistributions cannot be 0 in this method
			}
		}
		outlierCount--;
	}
}

void delim_to_array(float* data[], string filename, int length, int width, char delimiter) {
	string line;
	int row = 0, col = 0;
	ifstream file;
	file.open(filename);

	if (file.is_open()) {
		while (getline(file, line)) {
			if (row >= length) {
				std::cerr << "delim_to_array: Input file truncated (rows exceed " << length << ")" << std::endl;
				std::cin.get(); // Wait before exit
				exit(EXIT_FAILURE);
			}
			std::stringstream lineStream(line);
			std::string item;
			col = 0;
			while (getline(lineStream, item, delimiter)) {
				if (col >= width) {
					std::cerr << "delim_to_array: Dimensions mismatch on row " << row << " (columns exceed " << width << ")" << std::endl;
					std::cin.get(); // Wait before exit
					exit(EXIT_FAILURE);
				}
				(*data)[row*width+col] = fromString<float>(item);
				col++;
			}
			row++;
		}

		file.close();
	} else {
		std::cerr << "delim_to_array: Failed to open " << filename << std::endl;
		std::cerr << "delim_to_array: " << strerror(errno) << std::endl;
		std::cin.get(); // Wait before exit
		exit(EXIT_FAILURE);
	}
}

void csv_to_array(float* data[], string filename, int length, int width) {
	delim_to_array(data, filename, length, width, ',');
}

void bin_to_array(float* data[], string filename, int rows, int cols) {
	std::size_t size;
	std::filebuf* fbuffer;
	int* buffer;
	ifstream file;
	file.open(filename, ifstream::binary);

	if (file.is_open()) {
		buffer = (int *)malloc(sizeof(int)*rows*cols); // Assuming 4 byte ints
		if (buffer == nullptr) {
			std::cerr << "bin_to_array: Failed allocate buffer" << std::endl;
			std::cin.get(); // Wait before exit
			exit(EXIT_FAILURE);
		}

		file.read(reinterpret_cast<char *>(buffer), sizeof(int)*rows*cols);

		/* Slow conversion into floats */
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				(*data)[i*cols+j] =  (float)buffer[i*cols+j];
			}
		}

		file.close();
		free(buffer);
	} else {
		std::cerr << "bin_to_array: Failed to open " << filename << std::endl;
		std::cerr << "bin_to_array: " << strerror(errno) << std::endl;
		std::cin.get(); // Wait before exit
		exit(EXIT_FAILURE);
	}  
}

void normalize_data_array(float* data[], int length, int width) {
	if (length > 1) {
		float maxVal, minVal, tmp;
		int row, col;
	
		/* Find min/max value by col */
		for (col = 0; col < width; col++) {
			maxVal = (*data)[col];
			minVal = maxVal;
			for (row = 1; row < length; row++) {
				tmp = (*data)[row*width+col];
				maxVal = std::max(tmp,maxVal);
				minVal = std::min(tmp,minVal);
			}
	
			/* Normalization pass of this col */
			for (row = 0; row < length; row++) {
				(*data)[row*width+col] = (((*data)[row*width+col]-minVal)/(maxVal-minVal));
			}
		}
	}
}

/* Knuth Fisher Yates shuffle of columns (features) */
void shuffle_array(float *data[], int length, int width, unsigned int seed) {
	float tmp;
	int row, col, target;

	if (seed != NULL) {
		srand(seed);
	}

	for (col = width - 1; col > 0; col--) {
		target = (rand() % width);
		if (target != col) {
			for (row = 0; row < length; row++) {
				tmp = (*data)[row*width+col];
				(*data)[row*width+col] = (*data)[row*width+target];
				(*data)[row*width+target] = tmp;
			}
		}
	}
}

void calculate_stdev(float *data[], const int length, const int width, float *stdev[], float *mean[]) {
	int n, row, col;
	float delta, x, m, m2;

	/* Calculate by col */
	for (col = 0; col < width; col++) {
		n = 0;
		delta = 0.0f;
		m = 0.0f;
		m2 = 0.0f;

		for (row = 0; row < length; row++) {
			n++;
			x = (*data)[row*width+col];
			delta = x - m;
			m += delta/n;
			m2 += delta*(x-m);
		}

		(*stdev)[col] = sqrt(m2/(n-1));
		if (mean != NULL) {
			(*mean)[col] = m;
		}
	}
}

void random_uncertainty(float *data[], const int length, const int width, float *stdev[], float minUncertainty, float maxUncertainty) {
	calculate_stdev(data, length, width, stdev, NULL);
	/* Apply a random modifier to each objects' sd */
	for (int row = length-1; row >= 0; row--) {
		for (int col = 0; col < width; col++) {
			(*stdev)[row*width+col] = (*stdev)[col] * ((uniform_rand()*(maxUncertainty-minUncertainty+1.0f)+maxUncertainty));
		}
	}
}

/* Switch between row-major/column-major 2D arrays */
void switch_array_order(float *data[], float *dataR[], const int length, const int width) {
	for (int i = 0; i < length; i++) {
		for (int d = 0; d < width; d++) {
			(*dataR)[d*length+i] = (*data)[i*width+d];
		}
	}
}
