#ifndef KNN_H
#define KNN_H

void serial_distances(float *data[], float *distances[], int n, int r, int k, float *query[], int q);
void knn_distance(float *data[], float *distances[], int n, int r, int k, float *query[], int q, bool useOpenCL, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);
void knnt_distance(float *data[], float *distances[], float *thresholds[], int n, int r, int k, float *query[], int q, bool useOpenCL, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

#endif