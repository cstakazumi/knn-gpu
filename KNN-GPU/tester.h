#ifndef TESTER_H
#define TESTER_H

#include <string>
using std::string;

void check_results(string filename, int arrayLength, float probsArray[], float delta);
void check_synth_results(int length, float outlierRate, float probsArray[], float delta);
void simple_recall(int groundTruth[], int test[], int n, int q);

#endif