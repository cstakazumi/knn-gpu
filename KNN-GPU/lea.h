#ifndef LEA_H
#define LEA_H

#ifdef __cplusplus
extern "C" {
#endif

	double random_normal();
	double stdnormal_pdf(double x);
	double normal_pdf(double x, double mean, double vari);
	float normal_pdf_float(float x, float mean, float vari);
	double quad8_stdnormal_pdf(double a, double b);
	double stdnormal_cdf(double u);
	double stdnormal_inv(double p);
	double normal_inv(double p, double mean, double vari);
	float stdnormal_inv_float(float p);
	float normal_inv_float(float p, float mean, float vari);
	int choose(int n, int k);
	void combination(int *c[], int n, int p, int x);
	//void searchspace(int *space[], int n, int p);

#ifdef __cplusplus
}
#endif

#endif
