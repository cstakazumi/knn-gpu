#include <iostream>
#include <iomanip>
#include "main.h"
#include "opencl.h"
#include "sort.h"
#include "lea.h"
#include "timer.h"

/* Return squared distance */
inline float query_distance(float *data[], float *query[], int i, int j, int r, int n, int q) {
	float sqDistance = 0.0f;
	for (int d = 0; d < r; d++) {
		sqDistance += pow(((*data)[i*r+d] - (*query)[d*q+j]), 2);
	}
	return sqDistance;
}

/* Distances array size = n*q */
void serial_distances(float *data[], float *distances[], int n, int r, int k, float *query[], int q) {
	for (int i = 0; i < n; i++) { // For each object
		for (int d = 0; d < q; d++) { // For each query point
			(*distances)[i*q+d] = query_distance(data, query, i, d, r, n, q);
		}
	}
}

void knn_distance(float *data[], float *distances[], int n, int r, int k, float *query[], int q, bool useOpenCL, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	if (!useOpenCL) {
		DBG(std::cout << "KNN [distance] with CPU." << std::endl;);
#if TIMER
		time_t ltime;
		time(&ltime);
		double timeElapsed = 0.0;
		Timer *t;
		t = new Timer();
#endif
		serial_distances(data, distances, n, r, k, query, q);
#if TIMER
		timeElapsed = t->elapsed();
		std::cout << "CPU serial_distances time: " << timeElapsed << "s" << std::endl;
		delete t;
#endif
	} else {
		DBG(std::cout << "KNN [distance] with OpenCL " << ( useGPU ? "(GPU)." : "(CPU)." ) << std::endl;);
		//opencl_distances(data, distances, n, r, k, query, q, useGPU, openclDevice, openclPlatform, workgroupSize, workgroupCount);
		opencl_cached_distances(data, distances, n, r, k, query, q, useGPU, openclDevice, openclPlatform, workgroupSize, workgroupCount);
	}
}


void knnt_distance(float *data[], float *distances[], float *thresholds[], int n, int r, int k, float *query[], int q, bool useOpenCL, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	if (!useOpenCL) {
		DBG(std::cout << "KNN [distance] with CPU." << std::endl;);
#if TIMER
		time_t ltime;
		time(&ltime);
		double timeElapsed = 0.0;
		Timer *t;
		t = new Timer();
#endif
		serial_distances(data, distances, n, r, k, query, q);
#if TIMER
		timeElapsed = t->elapsed();
		std::cout << "CPU serial_distances time: " << timeElapsed << "s" << std::endl;
		delete t;
#endif
	} else {
		DBG(std::cout << "KNN [distance] with OpenCL " << ( useGPU ? "(GPU)." : "(CPU)." ) << std::endl;);
		opencl_threshold_distances(data, distances, thresholds, n, r, k, query, q, useGPU, openclDevice, openclPlatform, workgroupSize, workgroupCount);
	}
}
