#ifndef SORT_H
#define SORT_H

void ksort(float *data[], int *index[], int n, int q, int k, bool useOpenCL, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);
void mksort(float *data[], float *maxval[], int n, int q, int k, bool useOpenCL, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

#endif
