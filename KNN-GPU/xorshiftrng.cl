/* Extremely basic 3-xorshift RNG */
inline unsigned int xorshift(unsigned int seed) 
{
	seed++;
    seed ^= (seed << 17);
    seed ^= (seed >> 15);
    return seed ^= (seed << 26);
}

inline float xorshift_float(unsigned int seed)
{
	return native_divide((float)xorshift(seed),(float)((unsigned int)-1));
}
