/*
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

#include <string>
#include <iostream>
#include <cmath>
#include <ctime>
#include <inttypes.h>
#include "main.h"
#include "data.h"
#include "lea.h"
#include "tester.h"
#include "timer.h"
#include "opencl.h"
#include "xorshiftrng.h"
#include "sort.h"
#include "knn.h"

int main(int argc, char *argv[]) {
	//srand((unsigned int)time(NULL));
	srand((unsigned int)123456); // Use the same seed for testing

	float *data, *dataRotated, *distances, *queries, *queriesRotated, *dataTrimmed;

	/* Data */
	string inputFilename = "", truthFilename = "", queryFilename = "";
	bool inputTypeBinary;
	char inputDelimiter = ','; // For parsing files
	int syntheticDistributions; // For synthetic data generation

	/* Other variables */
	static int64_t n, r, q, k, trimmedn; // total rows, total cols, microclusters, query points, neighbors
	int s;
	bool success;

	bool dataRotate, distancePrune, matrixDistance;
	int OpenCLTarget, OpenCLPlatform, OpenCLDevice, OpenCLWorkgroupSize, OpenCLWorkgroupCount;
	bool OpenCLGPU = false, OpenCLZCPath = false, OpenCLForceCPUSort = false, OpenCLForceCPUPrune = true;

#if TIMER
	time_t ltime;
	time(&ltime);
	double timeElapsed = 0.0;
	Timer *t;
#endif

	/* Parameters */
	n = 262144; // Size of dataset (rows
	r = 512; // Number of dimensions (columns)
	k = 64; // Nearest neighbors
	q = 512; // Number of queries
	s = 8*k; // Number of samples to scan: k <= s <= n
	trimmedn = n; // Trim the dataset
	dataRotate = true;
	distancePrune = true;
	matrixDistance = false; 
	OpenCLTarget = 3; // Disabled = 0, CPU = 1, AMD = 2, NV = 3
	OpenCLForceCPUPrune = true; // Use CPU OpenCL pruning
	OpenCLForceCPUSort = true; // Use CPU OpenCL sort
	OpenCLZCPath = false;

	/* dataset */
	syntheticDistributions = 1; // Control the number of clusters in random data
	/* Only valid if syntheticDistributions is 0 */
	inputFilename = "Datasets\\NUSWIDE_BoW_int.dat";
	queryFilename = "";
	inputTypeBinary = false; // Parse text or bin
	inputDelimiter = ' '; // Only valid if not binary

	/* Targets */
	switch (OpenCLTarget) {
	case 1: // CPU
		OpenCLGPU = false;
		OpenCLPlatform = AMD_OPENCLPLATFORM;
		OpenCLDevice = CPU_OPENCLDEVICE;
		OpenCLWorkgroupSize = CPU_WORKGROUP_SIZE;
		OpenCLWorkgroupCount = CPU_WORKGROUP_COUNT;
		break;
	case 2: // AMD GPU
		OpenCLGPU = true;
		OpenCLPlatform = AMD_OPENCLPLATFORM;
		OpenCLDevice = AMD_OPENCLDEVICE;
		OpenCLWorkgroupSize = AMD_WORKGROUP_SIZE;
		OpenCLWorkgroupCount = AMD_WORKGROUP_COUNT;
		break;
	case 3: // NV GPU
		OpenCLGPU = true;
		OpenCLPlatform = NV_OPENCLPLATFORM;
		OpenCLDevice = NV_OPENCLDEVICE;
		OpenCLWorkgroupSize = NV_WORKGROUP_SIZE;
		OpenCLWorkgroupCount = NV_WORKGROUP_COUNT;
		break;
	default:
		OpenCLGPU = false;
		OpenCLPlatform = -1;
		OpenCLDevice = -1;
		OpenCLWorkgroupSize = -1;
		OpenCLWorkgroupCount = -1;
	}

	data = (float *)malloc((int64_t)sizeof(float)*n*r); check_alloc(data);
	queries = (float *)malloc((int64_t)sizeof(float)*q*r); check_alloc(queries);
	if (syntheticDistributions == 1) {
		synthetic_data_array(&data, n, r, 0.0f, 0.0f, 0); // Generate synthetic data
		synthetic_data_array(&queries, q, r, 0.0f, 0.0f, 0); // Generate synthetic query points
	} else if (syntheticDistributions > 1) {
		synthetic_data_array_clusters(&data, n, r, 0.0f, 0.0f, 0, 0); // Generate synthetic data
		synthetic_data_array_clusters(&queries, q, r, 0.0f, 0.0f, 0, 0); // Generate synthetic query points
	} else { // Read in data
		if (!inputTypeBinary) {
			delim_to_array(&data, inputFilename, n, r, inputDelimiter);
			if (queryFilename != "") csv_to_array(&queries, queryFilename, q, r);
			else synthetic_data_array(&queries, q, r, 0.0f, 0.0f, 0); // Generate synthetic query points
		} else {
			bin_to_array(&data, inputFilename, n, r);
			if (queryFilename != "") bin_to_array(&queries, queryFilename, q, r);
			else synthetic_data_array(&queries, q, r, 0.0f, 0.0f, 0); // Generate synthetic query points
		}
	}

	if (trimmedn < n) {
		dataTrimmed = (float *)malloc((int64_t)sizeof(float)*trimmedn*r); check_alloc(dataTrimmed);
		for (int i = 0; i < trimmedn; i++) {
			for (int j = 0; j < r; j++) {
				dataTrimmed[i*r+j] = data[i*r+j];
			}
		}
		free(data);
		data = dataTrimmed;
		n = trimmedn;
	}

	normalize_data_array(&data, n, r); // Normalize by col
	normalize_data_array(&queries, q, r); // Normalize by col

	if (dataRotate) {
		dataRotated = (float *)malloc((int64_t)sizeof(float)*n*r); check_alloc(dataRotated);
		switch_array_order(&data, &dataRotated, n, r);
		free(data);
		data = dataRotated;
		queriesRotated = (float *)malloc((int64_t)sizeof(float)*q*r); check_alloc(queriesRotated);
		switch_array_order(&queries, &queriesRotated, q, r);
		free(queries);
		queries = queriesRotated;
	}

#if !BATCH
	if (!OpenCLTarget) {
		std::cout << "Platform:   CPU (C++)" << std::endl;
	} else {
		std::cout << "Platform:   " << (!OpenCLGPU ? "CPU" : "GPU") << " (OpenCL target " << OpenCLTarget << ")" << std::endl;
	}
	if (!syntheticDistributions) {
		std::cout << "Data:       " << inputFilename << std::endl;
	} else {
		std::cout << "Data:       Synthetic" << std::endl;
	}
	std::cout << "Data size:  " << (sizeof(float)*n*r)/(1024*1024) << "MB (" << n << " x " << r << ")" << std::endl;
	std::cout << "Query size: " << (sizeof(float)*q*r)/(1024*1024) << "MB (" << q << " x " << r << ")" << std::endl;
	std::cout << "Dist. size: " << (sizeof(float)*n*q)/(1024*1024) << "MB (" << n << " x " << q << ")" << std::endl;
	std::cout << "Index size: " << (sizeof(float)*k*q)/(1024*1024) << "MB (" << k << " x " << q << ")" << std::endl;
	std::cout << "Total:      " << ((sizeof(float)*n*r) + (sizeof(float)*q*r) + (sizeof(float)*n*q) + (sizeof(float)*k*q))/(1024*1024) << "MB" << std::endl;
#endif

	float *bdistances = (float *)malloc((int64_t)sizeof(float)*n*q); check_alloc(bdistances);
	//int *bindex = (int *)calloc(n*q, sizeof(int)); check_alloc(bindex);
	int *knn = (int *)malloc(sizeof(int)*k*q); check_alloc(knn);

	/* For sampling */
	float* dataSample = (float *)malloc(sizeof(float)*s*r); check_alloc(dataSample);
	float* sdistances = (float *)malloc(sizeof(float)*s*q); check_alloc(sdistances);
	float* thresholds = (float *)malloc(sizeof(int)*q); check_alloc(thresholds);

#if TIMER
	std::cout << std::endl;
	t = new Timer();
#endif
	if (distancePrune) {
		for (int i = 0; i < s; i++) {
			for (int j = 0; j < r; j++) {
				//dataSample[i*r+j] = data[i*r+j];
				dataSample[j*s+i] = data[j*n+i];
			}
		}
		knn_distance(&dataSample, &sdistances, s, r, k, &queries, q, OpenCLTarget, OpenCLGPU, OpenCLDevice, OpenCLPlatform, OpenCLWorkgroupSize, OpenCLWorkgroupCount);

		mksort(&sdistances, &thresholds, s, q, k, true, false, CPU_OPENCLDEVICE, AMD_OPENCLPLATFORM, CPU_WORKGROUP_SIZE, CPU_WORKGROUP_COUNT);

		if (matrixDistance) {
			opencl_matrix_distances(&data, &bdistances, n, r, k, &queries, q, OpenCLGPU, OpenCLDevice, OpenCLPlatform, OpenCLWorkgroupSize, OpenCLWorkgroupCount);
		} else {
			//knn_distance(&data, &bdistances, n, r, k, &queries, q, OpenCLTarget, OpenCLGPU, OpenCLDevice, OpenCLPlatform, OpenCLWorkgroupSize, OpenCLWorkgroupCount);
			knnt_distance(&data, &bdistances, &thresholds, n, r, k, &queries, q, OpenCLTarget, OpenCLGPU, OpenCLDevice, OpenCLPlatform, OpenCLWorkgroupSize, OpenCLWorkgroupCount);
		}

		opencl_minmaxheapsort_threshold(&bdistances, &knn, &thresholds, n, q, k, false, CPU_OPENCLDEVICE, AMD_OPENCLPLATFORM, CPU_WORKGROUP_SIZE, CPU_WORKGROUP_COUNT); // Use CPU

		double tholdratio = 0.0;
		for (int d = 0; d < q; d++) {
			//std::cout << d << ": Threshold = " << thresholds[d] << " k-distance = " << bdistances[d*n+knn[(k-1)*q+d]] << std::endl;
			tholdratio += (bdistances[d*n+knn[(k-1)*q+d]] / thresholds[d]);
		}
		tholdratio /= q;
		std::cout << "Threshold ratio: " << tholdratio << std::endl;
	} else {
		if (OpenCLZCPath) {
			if (OpenCLTarget) {
				if (OpenCLForceCPUSort) opencl_dualc_knn(&data, &bdistances, &knn, &queries, n, r, k, q, OpenCLGPU, OpenCLDevice, OpenCLPlatform, OpenCLWorkgroupSize, OpenCLWorkgroupCount, false, CPU_OPENCLDEVICE, AMD_OPENCLPLATFORM, CPU_WORKGROUP_SIZE, CPU_WORKGROUP_COUNT); // Call directly
				else opencl_knn(&data, &bdistances, &knn, &queries, n, r, k, q, OpenCLGPU, OpenCLDevice, OpenCLPlatform, OpenCLWorkgroupSize, OpenCLWorkgroupCount); // Call directly
			}
			else {
				knn_distance(&data, &bdistances, n, r, k, &queries, q, false, false, 0, 0, 0, 0);
				ksort(&bdistances, &knn, n, q, k, false, false, 0, 0, 0, 0);
			}
		}
		else {
			if (OpenCLTarget) {
				knn_distance(&data, &bdistances, n, r, k, &queries, q, OpenCLTarget, OpenCLGPU, OpenCLDevice, OpenCLPlatform, OpenCLWorkgroupSize, OpenCLWorkgroupCount);
				ksort(&bdistances, &knn, n, q, k, OpenCLTarget, OpenCLGPU, OpenCLDevice, OpenCLPlatform, OpenCLWorkgroupSize, OpenCLWorkgroupCount);
			}
			else {
				knn_distance(&data, &bdistances, n, r, k, &queries, q, false, false, 0, 0, 0, 0);
				ksort(&bdistances, &knn, n, q, k, false, false, 0, 0, 0, 0);
			}
		}
	}
#if TIMER
	timeElapsed = t->elapsed();
	std::cout << "Total time: " << timeElapsed << "s" << std::endl;
	delete t;
#endif

#if DUMPINPUT
	std::cout << "==Data==" << std::endl;
	for (int i = 0; i < n; i++) {
		for (int d = 0; d < r; d++) {
			if (d == 0) {
				std::cout << data[i*r+d];
			}
			else {
				std::cout << "," << data[i*r+d];
			}
		}
		std::cout << std::endl;
	}

	std::cout << "==Queries==" << std::endl;
	for (int d = 0; d < r; d++) {
		for (int i = 0; i < q; i++) {
			if (i == 0) {
				std::cout << queries[d*q+i];
			}
			else {
				std::cout << "," << queries[d*q+i];
			}
		}
		std::cout << std::endl;
	}

	std::cout << "==Unsorted Distances==" << std::endl;
	for (int i = 0; i < n; i++) {
		for (int d = 0; d < q; d++) {
			if (d == 0) {
				std::cout << bdistances[d*n+i];
			}
			else {
				std::cout << "," << bdistances[d*n+i];
			}
		}
		std::cout << std::endl;
	}
#endif
#if DUMPOUTPUT
	std::cout << "==Sorted indexes==" << std::endl;
	for (int d = 0; d < q; d++) {
		for (int i = 0; i < k; i++) {
			if (i == 0) {
				std::cout << knn[i*q+d];
			} else {
				std::cout << "," << knn[i*q+d];
			}
		}
		std::cout << std::endl;
	}

	std::cout << "==Sorted Distances==" << std::endl;
	for (int d = 0; d < q; d++) {
		for (int i = 0; i < k; i++) {
			if (i == 0) {
				std::cout << bdistances[d*n+knn[i*q+d]];
			} else {
				std::cout << "," << bdistances[d*n+knn[i*q+d]];
			}
		}
		std::cout << std::endl;
	}
#endif

	free(bdistances); bdistances = nullptr;
	free(knn); knn = nullptr;

	free(data); data = nullptr;
	free(queries); queries = nullptr;

#if !BATCH
	std::cin.get();
#endif
	return EXIT_SUCCESS;
}
