/* Extremely basic 3-xorshift RNG */
unsigned int xorshift(unsigned int seed) 
{
	seed++;
    seed ^= (seed << 17);
    seed ^= (seed >> 15);
    return seed ^= (seed << 26);
}

/* Float value roughly in the range of (0,1) */
float xorshift_float(unsigned int seed)
{
	return ((float)xorshift(seed)/(unsigned int)-1);
}
