#include <iostream>
#include <string>
using std::string;
#include "data.h"
#include "main.h"

void simple_recall(int groundTruth[], int test[], int n, int q) {
	int hits;
	float recall;
	float avgRecall = 0.0f;
	std::cout << "Result:" << std::endl;
	for (int d = 0; d < q; d++) {
		hits = 0;
		recall = 0.0f;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (test[d*n+j] == groundTruth[d*n+i]) {
					hits++;
					break;
				}
			}
		}
		std::cout << "Query " << d+1 << ": Recall = " << (float)hits/(float)n << std::endl;
		avgRecall += (float)hits/(float)n;
	}
	avgRecall /= q;
	std::cout << "Average recall = " << avgRecall << std::endl;
}

void compare_truth(float probsArray[], float truthArray[], int length, float delta) {
	int outlierCount = 0, count = 0, tPositive = 0, fPositive = 0, tNegative = 0, fNegative = 0;
	double precision = 0.0, recall = 0.0;
	for (int row = 0; row < length; row++) {
#if DUMPOUTPUT
		std::cout << "Object " << row << " has probability " << probsArray[row];
#endif
		if (probsArray[row] < delta) {
			if (truthArray[row] == 1.0f) { // True Positive
				tPositive++;
#if DUMPOUTPUT				
				std::cout << " (true positive)";
#endif
			} else { // False Positive
				fPositive++;
#if DUMPOUTPUT
				std::cout << " (false positive)";
#endif
			}
			outlierCount++;
		} else {
			if (truthArray[row] == 1.0) { // False Negative
				fNegative++;
#if DUMPOUTPUT
				std::cout << " (false negative)";
#endif
			} else { // True Negative
				tNegative++;
#if DUMPOUTPUT
				//std::cout << " (true negative)";
#endif
			}
		}
		count++;
#if DUMPOUTPUT
		std::cout << std::endl;
#endif
	}

	if (tPositive > 0) {
		precision = (double)tPositive/((double)tPositive+(double)fPositive);
		recall = (double)tPositive/((double)tPositive+(double)fNegative);
	} else {
		precision = 0.0;
		recall = 0.0;
	}

	std::cout << "Result:    Precision = " << precision << ", Recall = " << recall << std::endl;
}

void check_results(string filename, int arrayLength, float probsArray[], float delta) {
	float *truthArray = (float *)malloc(sizeof(float)*arrayLength*1); check_alloc(truthArray);
	csv_to_array(&truthArray, filename, arrayLength, 1);
	if (truthArray == NULL) {
		std::cerr << "check_results: Failed to read in " << filename << std::endl;
	}
	compare_truth(probsArray, truthArray, arrayLength, delta);
	free(truthArray);
}

void check_synth_results(int length, float outlierRate, float probsArray[], float delta) {
	int outlierCount = (int)(length * outlierRate);
	if (outlierCount > length || outlierCount < 0) {
		std::cerr << "check_synth_results: Invalid outlier rate" << std::endl;
		return;
	}

	float *truthArray = (float *)calloc(length, sizeof(float));
	for (int row = 0; row < length; row++) {
		if (outlierCount > 0) {
			truthArray[row] = 1.0f;
			outlierCount--;
		}
		else truthArray[row] = 0.0f;
	}

	compare_truth(probsArray, truthArray, length, delta);
}
