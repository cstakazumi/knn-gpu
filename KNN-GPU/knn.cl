//#pragma OPENCL EXTENSION cl_amd_printf : enable

#if SQRT
#define DST(x) native_sqrt(x)
#else
#define DST(x) x
#endif

/* Single object per workgroup */
/* offset: if q > wg size, enqueue multiple times with appropriate offset */
__kernel void parallel_distances_vector4_worker(__global const float data[], __global float distances[], const uint n, const uint r, __global const float queries[], const uint q, __local float ldata[], const uint offset) {
	const uint i = (uint)get_group_id(0); // Object
	const uint j = (uint)get_local_id(0)+offset; // Query
	const uint stride = (uint)get_local_size(0);
	float sqDistance = 0.0f;
	float4 vdata, vquery;

	/* Shared data object, read in parallel */
	for (uint d = j-offset; d < r; d += stride) {
		ldata[d] = data[mad24(i,r,d)];
	}
	barrier(CLK_LOCAL_MEM_FENCE);

	for (uint d = 0; d < r; d += 4) {
		vdata = (float4)(ldata[d+0], ldata[d+1], ldata[d+2], ldata[d+3]);	
		vquery = (float4)(queries[mad24(d,q,j)], queries[mad24(d+1,q,j)], queries[mad24(d+2,q,j)], queries[mad24(d+3,q,j)]);
		sqDistance += distance(vdata, vquery);
	}

	distances[mad24(j,n,i)] = DST(sqDistance); // Write out to global memory
	//distances[mad24(i,q,j)] = DST(sqDistance);
}

/* Single object per workgroup */
/* offset: if q > wg size, enqueue multiple times with appropriate offset */
__kernel void parallel_distances_cached_worker(__global const float data[], __global float distances[], const uint n, const uint r, __global const float queries[], const uint q, __local float ldata[], const uint offset) {
	const uint i = (uint)get_group_id(0); // Object
	const uint j = (uint)get_local_id(0)+offset; // Query

	/* Shared data object, read in parallel */
	float sqDistance = 0.0f;
	for (uint d = j-offset; d < r; d += get_local_size(0)) {
		ldata[d] = data[mad24(i,r,d)];
	}
	barrier(CLK_LOCAL_MEM_FENCE);

	for (uint d = 0; d < r; d++) {
		sqDistance += native_powr(fabs(ldata[d] - queries[mad24(d,q,j)]), 2.0f);
	}
	
	distances[mad24(j,n,i)] = DST(sqDistance); // Write out to global memory
}

/* Pack multiple objects into each workgroup */
__kernel void parallel_distances_cached_multi_worker(__global const float data[], __global float distances[], const uint n, const uint r, __global const float queries[], const uint q, __local float ldata[]) {
	const uint i = (uint)get_global_id(0) / q; // Object
	const uint j = (uint)get_local_id(0) % q; // Query
	const uint loffset = (i*r) - (get_group_id(0)*r*(get_local_size(0)/q));
	float sqDistance = 0.0f;

	if (i < n) {
		/* Shared data object, read in parallel */
		for (uint d = j; d < r; d += q) {
			ldata[d+loffset] = data[mad24(i,r,d)];
		}
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	if (i < n) {
		for (uint d = 0; d < r; d++) {
			sqDistance += native_powr(fabs(ldata[d+loffset] - queries[mad24(d,q,j)]), 2.0f);
		}
	
		distances[mad24(j,n,i)] = DST(sqDistance); // Write out to global memory
	}
}


/* With early reject */
/* Single object per workgroup */
/* offset: if q > wg size, enqueue multiple times with appropriate offset */
__kernel void parallel_distances_threshold_worker(__global const float data[], __global float distances[], __global float thresholds[], const uint n, const uint r, __global const float queries[], const uint q, __local float ldata[], const uint offset) {
	const uint i = (uint)get_group_id(0); // Object
	const uint j = (uint)get_local_id(0)+offset; // Query
	const float threshold = thresholds[j];
	const uint width = 10;
	const uint xwidth = max(r/width, (uint)1);
	float temp;

	/* Shared data object, read in parallel */
	float sqDistance = 0.0f;
	for (uint d = j-offset; d < r; d += get_local_size(0)) {
		ldata[d] = data[mad24(i,r,d)];
	}
	barrier(CLK_LOCAL_MEM_FENCE);

	uint d = 0;
	for (uint x = 0; (x < xwidth) & (sqDistance < threshold); x++) {
		for (uint y = 0; y < width; y++) {
			temp = ldata[d] - queries[mad24(d,q,j)];
			sqDistance += temp*temp;
			d++;
		}
	}
	
	distances[mad24(j,n,i)] = DST(sqDistance); // Write out to global memory
}

/* Pack multiple objects into each workgroup */
__kernel void parallel_distances_threshold_multi_worker(__global const float data[], __global float distances[], __global float thresholds[], const uint n, const uint r, __global const float queries[], const uint q, __local float ldata[]) {
	const uint i = (uint)get_global_id(0) / q; // Object
	const uint j = (uint)get_local_id(0) % q; // Query
	const uint loffset = (i*r) - (get_group_id(0)*r*(get_local_size(0)/q));
	float sqDistance = 0.0f;
	const float threshold = thresholds[j];
	float temp;
	uint width = 10;
	uint xwidth = r/width;

	if (i < n) {
		/* Shared data object, read in parallel */
		for (uint d = j; d < r; d += q) {
			ldata[d+loffset] = data[mad24(i,r,d)];
		}
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	uint d = 0;

	if (i < n) {
		for (uint x = 0; (x < xwidth) & (sqDistance <= threshold); x++) {
			for (uint y = 0; y < width; y++) {
				temp = ldata[d+loffset] - queries[mad24(d,q,j)];
				sqDistance += temp*temp;
				d++;
			}
		}
	
		distances[mad24(j,n,i)] = DST(sqDistance); // Write out to global memory
	}
}

/* https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2 */
__inline uint roundup_power2(uint v) {    v--;    v |= v >> 1;    v |= v >> 2;    v |= v >> 4;    v |= v >> 8;    v |= v >> 16;    v++;    return v;}

#define WARP_SIZE 32
__kernel void parallel_sum_squares_worker(__global const float input[], __global float output[], const uint n, const uint r, __local float scratch[]) {
	const uint dim = (uint)get_local_id(0);
	const uint idx = (uint)get_group_id(0);
	const uint paddedsize = roundup_power2(r);
	float temp = 0.0f;

	scratch[dim] = 0.0f;
	for (uint load = dim; load < paddedsize; load += get_local_size(0)) {
		temp = (load < r ? input[mad24(idx,r,load)] : 0.0f);
		scratch[dim] += temp*temp;
	}
	barrier(CLK_LOCAL_MEM_FENCE);

	/* Reduce */
	for (uint i = get_local_size(0)/2; i > WARP_SIZE; i>>=1) {
		if (dim < i) scratch[dim] += scratch[dim+i];
		barrier(CLK_LOCAL_MEM_FENCE);
	}

	/* Final reduction in warp/wavefront */
	scratch[dim] += scratch[dim+32];
	scratch[dim] += scratch[dim+16];
	scratch[dim] += scratch[dim+8];
	scratch[dim] += scratch[dim+4];
	scratch[dim] += scratch[dim+2];
	scratch[dim] += scratch[dim+1];

	if (dim == 0) {
		output[idx] = scratch[0];
	}
}

__kernel void parallel_matrix_distance_worker(__global float D[], __global const float Dv[], __global const float Qv[], const uint r) {
	const uint q = (uint)get_local_id(0);
	const uint n = (uint)get_group_id(0);

	D[mad24(n,r,q)] =  DST(Dv[n] - (2 * D[mad24(n,r,q)]) + Qv[q]);
}
