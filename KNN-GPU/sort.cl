
/* Local selection sort */
__kernel void parallel_selection_sort_index(__global const float *data, __global int *index, const int n, const int q, const int offset, __local float *ldata, __local float *lindex) {
	int worker = get_local_id(0); // Index in local workgroup
	int d = get_group_id(0); // Local offset to global array
	int newPos = 0; // New position
	float workerVal, targetVal; // Values

	/* Each worker copies attributes from global to local */
	workerVal = data[mad24(d,n,worker+offset)];
	ldata[worker] = workerVal;
	lindex[worker] = worker+offset;
	barrier(CLK_LOCAL_MEM_FENCE);

	for (int target = 0; target < n; target++) {
		targetVal = ldata[target];
		newPos += ((targetVal < workerVal || (targetVal == workerVal && target < worker)) ? 1 : 0);
	}

	lindex[newPos] = worker;
	barrier(CLK_LOCAL_MEM_FENCE);
	index[mad24(d,n,worker+offset)] = lindex[worker]; // Reference to new position
}


inline void swap(__local float data[], __local uint index[], const uint a, const uint b, const uint hsize) {
	float tmpVal = data[a];
	uint tmpIdx = index[a];
	data[a] = data[b];
	index[a] = index[b];
	data[b] = tmpVal;
	index[b] = tmpIdx;
}

/* minmax heapsort with fixed heap and linear initialization (local heap) */
inline uint ulog2(uint value) {
	uint result = 0;
	while (value) {
		value >>= 1;
		result++;
	}
	return result - 1;
}

inline uint heap_parent(const uint node) {
	return (node - 1) >> 1; // /2
}

inline uint heap_leftchild(const uint node) {
	return 2 * node + 1;
}

inline uint heap_rightchild(const uint node) {
	return 2 * node + 2;
}

inline bool heap_isminlevel(const uint node) {
	return (ulog2(node + 1) & 1) == 0; // % 2
}

inline bool heap_ismaxlevel(const uint node) {
	return !heap_isminlevel(node);
}

/* Just access index 0 directly */
inline uint heap_minidx(__local float heapData[], const uint hsize) {
	const uint offset = hsize * get_local_id(0); // Local buffer offset
	return heapData[0+offset];
}

inline uint heap_maxidx(__local float heapData[], const uint hsize) {
	const uint offset = hsize * get_local_id(0); // Local buffer offset
	if (hsize > 2) return (heapData[1+offset] > heapData[2+offset] ? 1 : 2);
	else if (hsize == 2) return 1;
	else return 0;
}

inline void heap_swap(__local float heapData[], __local uint heapIndex[], const uint a, const uint b, const uint hsize) {
	const uint offset = hsize * get_local_id(0); // Local buffer offset
	float tmpVal = heapData[a+offset];
	uint tmpIdx = heapIndex[a+offset];
	heapData[a+offset] = heapData[b+offset];
	heapIndex[a+offset] = heapIndex[b+offset];
	heapData[b+offset] = tmpVal;
	heapIndex[b+offset] = tmpIdx;
}

/* AMD freaks out if you have inner loops with arbitrary iterations */
int heap_tricklemin_once(__local float heapData[], __local uint heapIndex[], const uint root, const uint end, const uint hsize) {
	const uint offset = hsize * get_local_id(0); // Local buffer offset
	uint minChildIdx, minGChildIdx = (uint)-1;
	float minChildVal, minGChildVal = MAXFLOAT;

	if (heap_leftchild(root) < end) { // root has at least 1 child
		/* Check children */
		minChildIdx = heap_leftchild(root);
		minChildVal = heapData[minChildIdx+offset];
		if ((heap_rightchild(root) < end) && (minChildVal > heapData[heap_rightchild(root)+offset])) {
			minChildIdx = heap_rightchild(root);
			minChildVal = heapData[minChildIdx+offset];
		}

		/* Check grandchildren */
		if (heap_leftchild(heap_leftchild(root)) < end) {
			minGChildIdx = heap_leftchild(heap_leftchild(root));
			minGChildVal = heapData[minGChildIdx+offset];
			for (int i = heap_leftchild(heap_leftchild(root)) + 1; (i < end) & (i < heap_leftchild(heap_leftchild(root)) + 4); i++) {
				if (minGChildVal > heapData[i+offset]) {
					minGChildIdx = i;
					minGChildVal = heapData[i+offset];
				}
			}
		}


		if ((heap_leftchild(heap_leftchild(root)) >= end | minChildVal <= minGChildVal) && (minChildVal < heapData[root+offset])) {
			heap_swap(heapData, heapIndex, root, minChildIdx, hsize); // Swap parent and child
		} else if (minGChildVal < heapData[root+offset]) { // Manage grandchildren
			heap_swap(heapData, heapIndex, root, minGChildIdx, hsize);
			if (minGChildVal > heapData[heap_parent(minGChildIdx)+offset]) {
				heap_swap(heapData, heapIndex, heap_parent(minGChildIdx), minGChildIdx, hsize);
			}
			return minGChildIdx;
		}
	}
	return -1;
}

int heap_tricklemax_once(__local float heapData[], __local uint heapIndex[], const uint root, const uint end, const uint hsize) {
	const uint offset = hsize * get_local_id(0); // Local buffer offset
	uint maxChildIdx, maxGChildIdx = (uint)-1;
	float maxChildVal, maxGChildVal = -MAXFLOAT;

	if (heap_leftchild(root) < end) { // root has at least 1 child
		/* Check children */
		maxChildIdx = heap_leftchild(root);
		maxChildVal = heapData[maxChildIdx+offset];
		if ((heap_rightchild(root) < end) && (maxChildVal < heapData[heap_rightchild(root)+offset])) {
			maxChildIdx = heap_rightchild(root);
			maxChildVal = heapData[maxChildIdx+offset];
		}

		/* Check grandchildren */
		if (heap_leftchild(heap_leftchild(root)) < end) {
			maxGChildIdx = heap_leftchild(heap_leftchild(root));
			maxGChildVal = heapData[maxGChildIdx+offset];
			for (int i = heap_leftchild(heap_leftchild(root)) + 1; (i < end) & (i < heap_leftchild(heap_leftchild(root)) + 4); i++) {
				if (maxGChildVal < heapData[i+offset]) {
					maxGChildIdx = i;
					maxGChildVal = heapData[i+offset];
				}
			}
		}

		if ((heap_leftchild(heap_leftchild(root)) >= end | maxChildVal >= maxGChildVal) && (maxChildVal > heapData[root+offset])) {
			heap_swap(heapData, heapIndex, root, maxChildIdx, hsize); // Swap parent and child
		} else if (maxGChildVal > heapData[root+offset]) { // Manage grandchildren
			heap_swap(heapData, heapIndex, root, maxGChildIdx, hsize);
			if (maxGChildVal < heapData[heap_parent(maxGChildIdx)+offset]) {
				heap_swap(heapData, heapIndex, heap_parent(maxGChildIdx), maxGChildIdx, hsize);
			}
			return maxGChildIdx;
		}
	}
	return -1;
}

/* Turn an array into a min-max heap in linear time */
void heapify_minmax(__local float heapData[], __local uint heapIndex[], const uint hsize) {
	int level = (int)floor((float)(hsize - 2) / 2), next = level;

	while (level >= 0) {
		if (heap_isminlevel(next)) {
			next = heap_tricklemin_once(heapData, heapIndex, next, hsize, hsize);
		}
		else {
			next = heap_tricklemax_once(heapData, heapIndex, next, hsize, hsize);
		}
		if (next == -1) {
			level--;
			next = level;
		}
	}
}

/* Insert into fixed size heap by overwriting largest element */
/* Resulting heap is invalid until tricklemax is called */
int heap_insert(__local float heapData[], __local uint heapIndex[], const float newPt, const uint newIdx, const uint hsize) {
	const uint offset = hsize * get_local_id(0); // Local buffer offset
	uint maxIdx = heap_maxidx(heapData, hsize);

	if (heapData[maxIdx+offset] > newPt) { // Old largest element larger than new one
		heapData[maxIdx+offset] = newPt;
		heapIndex[maxIdx+offset] = newIdx;

		/* Compare with root */
		if (newPt < heapData[0+offset]) { // New smallest point
			heap_swap(heapData, heapIndex, 0, maxIdx, hsize);
		}

		return heap_tricklemax_once(heapData, heapIndex, maxIdx, hsize, hsize);
	} else return -1;
}

int heap_delete(__local float heapData[], __local uint heapIndex[], const uint k, const uint hsize) {
	/* Replace root with last element */
	const uint offset = hsize * get_local_id(0); // Local buffer offset
	heapData[0+offset] = heapData[k-1+offset];
	heapIndex[0+offset] = heapIndex[k-1+offset];

	return heap_tricklemin_once(heapData, heapIndex, 0, k, hsize);
}

/* Partial heapsort with separate local heap */
/* 1 workgroup = 1 query, each thread partitions data */
/* Local buffers length k */

/* Single worker per wg version (for large k) */
__kernel void parallel_minmaxheapsort_index(__global const float data[], __global uint index[], const uint n, const uint q, const uint k, __local float heapData[], __local uint heapIndex[]) {
	const uint d = get_group_id(0); // Query
	
	/* Initialize local array with first k elements */
	for (uint i = 0; i < k; i++) {
		heapData[i] = data[mad24(d, n, i)];
		heapIndex[i] = i;
	}

	heapify_minmax(heapData, heapIndex, k); // Create initial heap

	/* Iteratively insert remaining heap elements, starting at k */

	uint j = k;
	int update = -1;

	while (j < n) {
		if (update == -1) { 
			update = heap_insert(heapData, heapIndex, data[mad24(d, n, j)], j, k);
		}

		if (update != -1) {
			update = heap_tricklemax_once(heapData, heapIndex, update, k, k);
		} 
		
		if (update == -1) {
			j++;
		}
	}

	/* Output */
	j = 0;
	update = -1;
	while (j < k) {
		if (update == -1) {
			index[mad24(j, q, d)] = heapIndex[0];
			update = heap_delete(heapData, heapIndex, k-j, k);
		}
		
		if (update != -1) {
			update = heap_tricklemin_once(heapData, heapIndex, update, k-j, k);
		}

		if (update == -1) {
			j++;
		}
	}
}

/* Max val only */
__kernel void parallel_reduce_minmaxheapsort_max(__global const float data[], __global float maxval[], const uint n, const uint q, const uint k, __local float heapData[], __local uint heapIndex[]) {
	const uint loffset = get_local_id(0) * k; // Local buffer offset
	const uint gblocksize = (uint)ceil(native_divide((float)n, (float)get_local_size(0))); // Expected blocksize
	const uint pblocksize = min((uint)(get_local_id(0) * gblocksize) + gblocksize, n) - (get_local_id(0) * gblocksize);
	const uint d = get_group_id(0); // Query
	const uint pk = min(k, pblocksize); // The last worker might have less to do

	/* Initialize local array with first k elements */
	uint idx;
	for (uint i = 0; i < pk; i++) {
		idx = mad24(d, n, mad24(i,get_local_size(0),get_local_id(0)));
		heapData[i + loffset] = data[idx];
		heapIndex[i + loffset] = idx;
	}

	heapify_minmax(heapData, heapIndex, pk); // Create initial heap

	/* Iteratively insert remaining elements, starting at k */
	uint j = pk;
	int update = -1;
	while ((j < pblocksize) & ((idx = mad24(j,get_local_size(0),get_local_id(0))) < n)) {
		if (update == -1) {
			update = heap_insert(heapData, heapIndex, data[mad24(d, n, idx)], idx, pk);
		}

		if (update != -1) {
			update = heap_tricklemax_once(heapData, heapIndex, update, pk, pk);
		}

		if (update == -1) {
			j++;
		}
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	/* Reduce */
	if (get_local_id(0) == 0) {
		const uint allk = (k*(get_local_size(0)-1)) + min((uint)((gblocksize*get_local_size(0))-(gblocksize*(get_local_size(0)-1))),k);
#if REDUCE_ITERATIVE
		/* Iteratively insert remaining elements, starting at k */
		for (uint j = k; j < allk; j++) {
			heap_insert(heapData, heapIndex, heapData[j], heapIndex[j], k);
		}

		/* Output just top k */
		for (uint i = 0; i < k; i++) {
			index[mad24(i, q, d)] = heapIndex[0];
			heap_delete(heapData, heapIndex, k - i, k);
		}
#else
		/* Create one big heap */
		heapify_minmax(heapData, heapIndex, allk);

		/* Output just max */
		maxval[d] = heapData[heap_maxidx(heapData, allk)];
#endif
	}
}


/* Global memory heap (use CPU) */

inline uint gheap_minidx(__global float heapData[], const uint hsize) {
	const uint offset = hsize * get_global_id(0); // Heap offset
	return heapData[0+offset];
}

inline uint gheap_maxidx(__global float heapData[], const uint hsize) {
	const uint offset = hsize * get_global_id(0); // Heap offset
	if (hsize > 2) return (heapData[1+offset] > heapData[2+offset] ? 1 : 2);
	else if (hsize == 2) return 1;
	else return 0;
}

inline void gheap_swap(__global float heapData[], __global uint heapIndex[], const uint a, const uint b, const uint hsize) {
	const uint offset = hsize * get_global_id(0); // Heap offset
	float tmpVal = heapData[a+offset];
	uint tmpIdx = heapIndex[a+offset];
	heapData[a+offset] = heapData[b+offset];
	heapIndex[a+offset] = heapIndex[b+offset];
	heapData[b+offset] = tmpVal;
	heapIndex[b+offset] = tmpIdx;
}

/* AMD freaks out if you have inner loops with arbitrary iterations */
int gheap_tricklemin_once(__global float heapData[], __global uint heapIndex[], const uint root, const uint end, const uint hsize) {
	const uint offset = hsize * get_global_id(0); // Heap offset
	uint minChildIdx, minGChildIdx = (uint)-1;
	float minChildVal, minGChildVal = MAXFLOAT;

	if (heap_leftchild(root) < end) { // root has at least 1 child
		/* Check children */
		minChildIdx = heap_leftchild(root);
		minChildVal = heapData[minChildIdx+offset];
		if ((heap_rightchild(root) < end) && (minChildVal > heapData[heap_rightchild(root)+offset])) {
			minChildIdx = heap_rightchild(root);
			minChildVal = heapData[minChildIdx+offset];
		}

		/* Check grandchildren */
		if (heap_leftchild(heap_leftchild(root)) < end) {
			minGChildIdx = heap_leftchild(heap_leftchild(root));
			minGChildVal = heapData[minGChildIdx+offset];
			for (int i = heap_leftchild(heap_leftchild(root)) + 1; (i < end) & (i < heap_leftchild(heap_leftchild(root)) + 4); i++) {
				if (minGChildVal > heapData[i+offset]) {
					minGChildIdx = i;
					minGChildVal = heapData[i+offset];
				}
			}
		}


		if ((heap_leftchild(heap_leftchild(root)) >= end | minChildVal <= minGChildVal) && (minChildVal < heapData[root+offset])) {
			gheap_swap(heapData, heapIndex, root, minChildIdx, hsize); // Swap parent and child
		} else if (minGChildVal < heapData[root+offset]) { // Manage grandchildren
			gheap_swap(heapData, heapIndex, root, minGChildIdx, hsize);
			if (minGChildVal > heapData[heap_parent(minGChildIdx)+offset]) {
				gheap_swap(heapData, heapIndex, heap_parent(minGChildIdx), minGChildIdx, hsize);
			}
			return minGChildIdx;
		}
	}
	return -1;
}

int gheap_tricklemax_once(__global float heapData[], __global uint heapIndex[], const uint root, const uint end, const uint hsize) {
	const uint offset = hsize * get_global_id(0); // Heap offset
	uint maxChildIdx, maxGChildIdx = (uint)-1;
	float maxChildVal, maxGChildVal = -MAXFLOAT;

	if (heap_leftchild(root) < end) { // root has at least 1 child
		/* Check children */
		maxChildIdx = heap_leftchild(root);
		maxChildVal = heapData[maxChildIdx+offset];
		if ((heap_rightchild(root) < end) && (maxChildVal < heapData[heap_rightchild(root)+offset])) {
			maxChildIdx = heap_rightchild(root);
			maxChildVal = heapData[maxChildIdx+offset];
		}

		/* Check grandchildren */
		if (heap_leftchild(heap_leftchild(root)) < end) {
			maxGChildIdx = heap_leftchild(heap_leftchild(root));
			maxGChildVal = heapData[maxGChildIdx+offset];
			for (int i = heap_leftchild(heap_leftchild(root)) + 1; (i < end) & (i < heap_leftchild(heap_leftchild(root)) + 4); i++) {
				if (maxGChildVal < heapData[i+offset]) {
					maxGChildIdx = i;
					maxGChildVal = heapData[i+offset];
				}
			}
		}

		if ((heap_leftchild(heap_leftchild(root)) >= end | maxChildVal >= maxGChildVal) && (maxChildVal > heapData[root+offset])) {
			gheap_swap(heapData, heapIndex, root, maxChildIdx, hsize); // Swap parent and child
		} else if (maxGChildVal > heapData[root+offset]) { // Manage grandchildren
			gheap_swap(heapData, heapIndex, root, maxGChildIdx, hsize);
			if (maxGChildVal < heapData[heap_parent(maxGChildIdx)+offset]) {
				gheap_swap(heapData, heapIndex, heap_parent(maxGChildIdx), maxGChildIdx, hsize);
			}
			return maxGChildIdx;
		}
	}
	return -1;
}

/* Turn an array into a min-max heap in linear time */
void gheapify_minmax(__global float heapData[], __global uint heapIndex[], const uint hsize) {
	int level = (int)floor((float)(hsize - 2) / 2), next = level;

	while (level >= 0) {
		if (heap_isminlevel(next)) {
			next = gheap_tricklemin_once(heapData, heapIndex, next, hsize, hsize);
		}
		else {
			next = gheap_tricklemax_once(heapData, heapIndex, next, hsize, hsize);
		}
		if (next == -1) {
			level--;
			next = level;
		}
	}
}

/* Insert into fixed size heap by overwriting largest element */
/* Resulting heap is invalid until tricklemax is called */
int gheap_insert(__global float heapData[], __global uint heapIndex[], const float newPt, const uint newIdx, const uint hsize) {
	const uint offset = hsize * get_global_id(0); // Heap offset
	uint maxIdx = gheap_maxidx(heapData, hsize);

	if (heapData[maxIdx+offset] > newPt) { // Old largest element larger than new one
		heapData[maxIdx+offset] = newPt;
		heapIndex[maxIdx+offset] = newIdx;

		/* Compare with root */
		if (newPt < heapData[0+offset]) { // New smallest point
			gheap_swap(heapData, heapIndex, 0, maxIdx, hsize);
		}

		return gheap_tricklemax_once(heapData, heapIndex, maxIdx, hsize, hsize);
	} else return -1;
}

int gheap_delete(__global float heapData[], __global uint heapIndex[], const uint k, const uint hsize) {
	/* Replace root with last element */
	const uint offset = hsize * get_global_id(0); // Heap offset
	heapData[0+offset] = heapData[k-1+offset];
	heapIndex[0+offset] = heapIndex[k-1+offset];

	return gheap_tricklemin_once(heapData, heapIndex, 0, k, hsize);
}

/* Partial heapsort with separate global heap */
/* thread = query */
__kernel void parallel_global_minmaxheapsort_index(__global const float data[], __global uint index[], const uint n, const uint q, const uint k, __global float heapData[], __global uint heapIndex[]) {
	const uint d = get_global_id(0); // Query
	const uint offset = k * get_global_id(0); // Heap offset

	/* Initialize heap with first k elements */
	for (uint i = 0; i < k; i++) {
		heapData[i+offset] = data[mad24(d, n, i)];
		heapIndex[i+offset] = i;
	}

	gheapify_minmax(heapData, heapIndex, k); // Create initial heap

	/* Iteratively insert remaining heap elements, starting at k */

	uint j = k;
	int update = -1;

	while (j < n) {
		if (update == -1) { 
			update = gheap_insert(heapData, heapIndex, data[mad24(d, n, j)], j, k);
		}

		if (update != -1) {
			update = gheap_tricklemax_once(heapData, heapIndex, update, k, k);
		} 
		
		if (update == -1) {
			j++;
		}
	}

	/* Output */
	j = 0;
	update = -1;
	while (j < k) {
		if (update == -1) {
			index[mad24(j, q, d)] = heapIndex[0+offset];
			update = gheap_delete(heapData, heapIndex, k-j, k);
		}
		
		if (update != -1) {
			update = gheap_tricklemin_once(heapData, heapIndex, update, k-j, k);
		}

		if (update == -1) {
			j++;
		}
	}
}

/* Max val only */
__kernel void parallel_global_minmaxheapsort_max(__global const float data[], __global float maxval[], const uint n, const uint q, const uint k, __global float heapData[], __global uint heapIndex[]) {
	const uint d = get_global_id(0); // Query
	const uint offset = k * get_global_id(0); // Heap offset

	/* Initialize heap with first k elements */
	for (uint i = 0; i < k; i++) {
		heapData[i+offset] = data[mad24(d, n, i)];
		heapIndex[i+offset] = i;
	}

	gheapify_minmax(heapData, heapIndex, k); // Create initial heap

	/* Iteratively insert remaining heap elements, starting at k */
	uint j = k;
	int update = -1;

	while (j < n) {
		if (update == -1) { 
			update = gheap_insert(heapData, heapIndex, data[mad24(d, n, j)], j, k);
		}

		if (update != -1) {
			update = gheap_tricklemax_once(heapData, heapIndex, update, k, k);
		} 
		
		if (update == -1) {
			j++;
		}
	}

	/* Output max only */
	maxval[d] = heapData[gheap_maxidx(heapData, k)+offset];
}

/* With threshold */
__kernel void parallel_global_minmaxheapsort_threshold(__global const float data[], __global const float thresholds[], __global uint index[], const uint n, const uint q, const uint k, __global float heapData[], __global uint heapIndex[]) {
	const uint d = get_global_id(0); // Query
	const float threshold = thresholds[d];
	const uint offset = k * get_global_id(0); // Heap offset

	/* Initialize heap with first k elements (ignoring threshold for now) */
	for (uint i = 0; i < k; i++) {
		heapData[i+offset] = data[mad24(d, n, i)];
		heapIndex[i+offset] = i;
	}

	gheapify_minmax(heapData, heapIndex, k); // Create initial heap

	/* Iteratively insert remaining heap elements, starting at k */

	uint j = k;
	int update = -1;

	while (j < n) {
		if (data[mad24(d, n, j)] > threshold) {
			j++;
		} else {
			if (update == -1) { 
				update = gheap_insert(heapData, heapIndex, data[mad24(d, n, j)], j, k);
			}

			if (update != -1) {
				update = gheap_tricklemax_once(heapData, heapIndex, update, k, k);
			} 
		
			if (update == -1) {
				j++;
			}
		}
	}

	/* Output */
	j = 0;
	update = -1;
	while (j < k) {
		if (update == -1) {
			index[mad24(j, q, d)] = heapIndex[0+offset];
			update = gheap_delete(heapData, heapIndex, k-j, k);
		}
		
		if (update != -1) {
			update = gheap_tricklemin_once(heapData, heapIndex, update, k-j, k);
		}

		if (update == -1) {
			j++;
		}
	}
}
