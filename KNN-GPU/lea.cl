/* Double support for AMD or other */
#if defined(cl_khr_fp64)
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#elif defined(cl_amd_fp64)
#pragma OPENCL EXTENSION cl_amd_fp64 : enable
#endif

/* M_ constants (float ver) */
#ifndef M_E
#define M_E (2.718282)
#endif
#ifndef M_LOG2E
#define M_LOG2E (1.442695)
#endif
#ifndef M_LOG10E
#define M_LOG10E (0.434294)
#endif
#ifndef M_LN2
#define M_LN2 (0.693147)
#endif
#ifndef M_LN10
#define M_LN10 (2.302585)
#endif
#ifndef M_PI
#define M_PI (3.14159)
#endif
#ifndef M_PI_2
#define M_PI_2 (1.570796)
#endif
#ifndef M_PI_4
#define M_PI_4 (0.785398)
#endif
#ifndef M_1_PI
#define M_1_PI (0.318310)
#endif
#ifndef M_2_PI
#define M_2_PI (0.636620)
#endif
#ifndef M_1_SQRTPI
#define M_1_SQRTPI (0.564190)
#endif
#ifndef M_2_SQRTPI
#define M_2_SQRTPI (1.128379)
#endif
#ifndef M_SQRT2
#define M_SQRT2 (1.414214)
#endif
#ifndef M_SQRT2PI
#define M_SQRT2PI (2.5066284)
#endif
#ifndef M_SQRT_2
#define M_SQRT_2 (0.707107)
#endif

/* Flags */
#define NAN_D 0x7ff8000000000000
#define NNAN_D 0xfff8000000000000
#define INF_D 0x7ff0000000000000
#define NINF_D 0xfff0000000000000

#define NAN_F 0x7fc00000
#define NNAN_F 0xffc00000
#define INF_F 0x7f800000
#define NINF_F 0xff800000

/*
 * lea.c by Jeremy Lea
 * http://home.online.no/~pjacklam/notes/invnorm/impl/lea/lea.c
 * Modified and ported to OpenCL by Takazumi Matsumoto
 */


/*
 * The standard normal PDF, for one random variable.
 */
double stdnormal_pdf(double x)
{
	return exp(-x*x/2)/M_SQRT2PI;
}

double normal_pdf(double x, double mean, double vari)
{
	return exp((-(x-mean)*(x-mean))/(2*vari*vari))/(M_SQRT2PI*vari);
}

float normal_pdf_float(float x, float mean, float vari)
{
	return native_divide(native_exp(native_divide((-(x-mean)*(x-mean)),2.0f*native_powr(vari,2.0f))),(float)(M_SQRT2PI*vari));
}

double4 normal_pdf_double4(double4 x, double4 mean, double4 vari)
{
	return exp((-(x-mean)*(x-mean))/(2*vari*vari))/(M_SQRT2PI*vari);
}

float4 normal_pdf_float4(float4 x, float4 mean, float4 vari)
{
	return native_divide(native_exp(native_divide((-(x-mean)*(x-mean)),2.0f*native_powr(vari,2.0f))),((float)M_SQRT2PI*vari));
}

/*
 * An implementation of adaptive, recursive Newton-Cotes integration.
 * Based on the MATLAB implementation, but covered in a lot of books...
 *
 * This only does integration over the standard normal PDF.  It's just
 * here to check the error function approximations.
 */
#define LEVMAX 10

/* Secondary function here for refinement only, avoid recursion in OpenCL */
double quad8_stdnormal_pdfEx(double a, double b, double Q)
{
/* The magic Newton-Cotes weights */
	const int w[9] = {3956, 23552, -3712, 41984, -18160, 41984, -3712, 23552, 3956};
	const int dw = 14175;

	double h, Q1 = 0.0, Q2 = 0.0;
	int i;
	h = (b-a)/16.0;
	for (i = 0; i < 9; i++) 
	{
		Q1 += h*w[i]*stdnormal_pdf(a+i*h)/dw;
		Q2 += h*w[i]*stdnormal_pdf(a+(i+8)*h)/dw;
	}

	return Q1 + Q2;
}

double quad8_stdnormal_pdf(double a, double b)
{
	double Q = 1.0;
	/* The magic Newton-Cotes weights */
	const int w[9] = {3956, 23552, -3712, 41984, -18160, 41984, -3712, 23552, 3956};
	const int dw = 14175;
	int level = -1;
	double tol = 1e-30;

	double h, Q1 = 0.0, Q2 = 0.0;
	int i;

	level++;
	h = (b-a)/16.0;
	for (i = 0; i < 9; i++) 
	{
		Q1 += h*w[i]*stdnormal_pdf(a+i*h)/dw;
		Q2 += h*w[i]*stdnormal_pdf(a+(i+8)*h)/dw;
	}
	/* This is (or rather was) the adaptive recursive bit */
	while (fabs(Q1+Q2-Q) > tol*fabs(Q1+Q2) && level <= LEVMAX) {
		tol = tol/2;
		Q1 = quad8_stdnormal_pdfEx(a,(a+b)/2,Q1);
		Q2 = quad8_stdnormal_pdfEx((a+b)/2,b,Q2);
		tol = tol*2;
	}
	level--;

	return Q1 + Q2;
}

/*
 * The standard normal CDF, for one random variable.
 *
 *   Author:  W. J. Cody
 *   URL:   http://www.netlib.org/specfun/erf
 *
 * This is the erfc() routine only, adapted by the
 * transform stdnormal_cdf(u)=(erfc(-u/sqrt(2))/2;
 */
double stdnormal_cdf(double u)
{
	const double a[5] = {
	1.161110663653770e-002,3.951404679838207e-001,2.846603853776254e+001,
	1.887426188426510e+002,3.209377589138469e+003
	};
	const double b[5] = {
	1.767766952966369e-001,8.344316438579620e+000,1.725514762600375e+002,
	1.813893686502485e+003,8.044716608901563e+003
	};
	const double c[9] = {
	2.15311535474403846e-8,5.64188496988670089e-1,8.88314979438837594e00,
	6.61191906371416295e01,2.98635138197400131e02,8.81952221241769090e02,
	1.71204761263407058e03,2.05107837782607147e03,1.23033935479799725E03
	};
	const double d[9] = {
	1.00000000000000000e00,1.57449261107098347e01,1.17693950891312499e02,
	5.37181101862009858e02,1.62138957456669019e03,3.29079923573345963e03,
	4.36261909014324716e03,3.43936767414372164e03,1.23033935480374942e03
	};
	const double p[6] = {
	1.63153871373020978e-2,3.05326634961232344e-1,3.60344899949804439e-1,
	1.25781726111229246e-1,1.60837851487422766e-2,6.58749161529837803e-4
	};
	const double q[6] = {
	1.00000000000000000e00,2.56852019228982242e00,1.87295284992346047e00,
	5.27905102951428412e-1,6.05183413124413191e-2,2.33520497626869185e-3
	};
	double y, z;

	if (isnan(u))
		return NAN_D;
	if (!isfinite(u))
		return (u < 0 ? 0.0 : 1.0);
		y = fabs(u);
	if (y <= 0.46875*M_SQRT2) {
		/* evaluate erf() for |u| <= sqrt(2)*0.46875 */
		z = y*y;
		y = u*((((a[0]*z+a[1])*z+a[2])*z+a[3])*z+a[4])/((((b[0]*z+b[1])*z+b[2])*z+b[3])*z+b[4]);
		return 0.5+y;
	}
	z = exp(-y*y/2)/2;
	if (y <= 4.0) {
		/* evaluate erfc() for sqrt(2)*0.46875 <= |u| <= sqrt(2)*4.0 */
		y = y/M_SQRT2;
		y = ((((((((c[0]*y+c[1])*y+c[2])*y+c[3])*y+c[4])*y+c[5])*y+c[6])*y+c[7])*y+c[8])/((((((((d[0]*y+d[1])*y+d[2])*y+d[3])*y+d[4])*y+d[5])*y+d[6])*y+d[7])*y+d[8]);
		y = z*y;
	} else {
		/* evaluate erfc() for |u| > sqrt(2)*4.0 */
		z = z*M_SQRT2/y;
		y = 2/(y*y);
		y = y*(((((p[0]*y+p[1])*y+p[2])*y+p[3])*y+p[4])*y+p[5])/(((((q[0]*y+q[1])*y+q[2])*y+q[3])*y+q[4])*y+q[5]);
		y = z*(M_1_SQRTPI-y);
	}
	return (u < 0.0 ? y : 1-y);
}

/*
 * The inverse standard normal distribution.
 *
 *   Author:      Peter John Acklam <pjacklam@online.no>
 *   URL:         http://home.online.no/~pjacklam
 *
 * This function is based on the MATLAB code from the address above,
 * translated to C, and adapted for our purposes.
 */
double stdnormal_inv(double p)
{
	const double a[6] = {
	-3.969683028665376e+01,  2.209460984245205e+02,
	-2.759285104469687e+02,  1.383577518672690e+02,
	-3.066479806614716e+01,  2.506628277459239e+00
	};
	const double b[5] = {
	-5.447609879822406e+01,  1.615858368580409e+02,
	-1.556989798598866e+02,  6.680131188771972e+01,
	-1.328068155288572e+01
	};
	const double c[6] = {
	-7.784894002430293e-03, -3.223964580411365e-01,
	-2.400758277161838e+00, -2.549732539343734e+00,
	4.374664141464968e+00,  2.938163982698783e+00
	};
	const double d[4] = {
	7.784695709041462e-03,  3.224671290700398e-01,
	2.445134137142996e+00,  3.754408661907416e+00
	};

	double q, t, u;

	if (isnan(p) || p > 1.0 || p < 0.0)
		return NAN_D;
	if (p == 0.0)
		return NINF_D;
	if (p == 1.0)
		return INF_D;
	q = min(p,1-p);
	if (q > 0.02425) {
		/* Rational approximation for central region. */
		u = q-0.5;
		t = u*u;
		u = u*(((((a[0]*t+a[1])*t+a[2])*t+a[3])*t+a[4])*t+a[5])/(((((b[0]*t+b[1])*t+b[2])*t+b[3])*t+b[4])*t+1);
	} else {
		/* Rational approximation for tail region. */
		t = sqrt(-2*log(q));
		u = (((((c[0]*t+c[1])*t+c[2])*t+c[3])*t+c[4])*t+c[5])/((((d[0]*t+d[1])*t+d[2])*t+d[3])*t+1);
	}
	/* The relative error of the approximation has absolute value less
	than 1.15e-9.  One iteration of Halley's rational method (third
	order) gives full machine precision... */
	//t = stdnormal_cdf(u)-q;    /* error */
	t = 0.5*erfc(-u/sqrt(2.0))-q; /* error (OpenCL erfc) */
	t = t*M_SQRT2PI*exp(u*u/2);   /* f(u)/df(u) */
	u = u-t/(1+u*t/2);     /* Halley's method */

	return (p > 0.5 ? -u : u);
}

double normal_inv(double p, double mean, double stdev)
{
	return stdnormal_inv(p)*stdev+mean;
}

/*
 * inverse cdf using only single precision
 */
float stdnormal_inv_float(float p)
{
	const float a[6] = {
	-3.969683e+01f,  2.209461e+02f,
	-2.759285e+02f,  1.383578e+02f,
	-3.066480e+01f,  2.506628e+00f
	};
	const float b[5] = {
	-5.447601e+01f,  1.615858e+02f,
	-1.556990e+02f,  6.680131e+01f,
	-1.328068e+01f
	};
	const float c[6] = {
	-7.784894e-03f, -3.223965e-01f,
	-2.400758e+00f, -2.549733e+00f,
	4.3746641e+00f,  2.938164e+00f
	};
	const float d[4] = {
	7.7846957e-03f,  3.224671e-01f,
	2.4451341e+00f,  3.754409e+00f
	};

	float q, t, u;

	if (isnan(p) || p > 1.0f || p < 0.0f)
		return NAN_F;
	if (p == 0.0f)
		return NINF_F;
	if (p == 1.0f)
		return INF_F;
	q = min(p,1.0f-p);
	
	if (q > 0.02425f) {
		/* Rational approximation for central region. */
		u = q-0.5f;
		t = u*u;
		u = u*native_divide(mad(mad(mad(mad(mad(a[0],t,a[1]),t,a[2]),t,a[3]),t,a[4]),t,a[5]),mad(mad(mad(mad(mad(b[0],t,b[1]),t,b[2]),t,b[3]),t,b[4]),t,1));
	} else {
		/* Rational approximation for tail region. */
		t = native_sqrt(-2*native_log(q));
		u = native_divide(mad(mad(mad(mad(mad(c[0],t,c[1]),t,c[2]),t,c[3]),t,c[4]),t,c[5]),mad(mad(mad(mad(d[0],t,d[1]),t,d[2]),t,d[3]),t,1));
	}

	/* No refinement required with single precision */
	return (p > 0.5f ? -u : u);
}

float normal_inv_float(float p, float mean, float stdev)
{
	return mad(stdnormal_inv_float(p),stdev,mean);
}

/* Binomial coefficient (n Choose k) */
int choose(int n, int k) {
	/*
    if (k > n) return 0;
    if (k * 2 > n) k = n-k;
    if (k == 0) return 1;

	int result = n;
    for (int i = 2; i <= k; ++i) {
        result *= (n-i+1);
        result /= i;
    }
    return result;
	*/
	return (int)(tgamma(n+1.0f)/(tgamma(k+1.0f)*(tgamma(n-k+1.0f))));
}

void combination(__local int c[], int n, int p, int x) {
    int r, k = 0;
	if (p == 1) {
		c[0] = x;
	} else {
		for (int i = 0; i < p-1; i++) {
			c[i] = (i != 0) ? c[i-1] : 0;
			do {
				c[i]++;
				r = choose(n-c[i], p-(i+1));
				k = k + r;
			} while(k < x);
			k = k - r;
		}
		c[p-1] = c[p-2] + x - k;
	}
}

void searchspace(__local int space[], __local int c[], int n, int p) {
	for (int x = 1; x <= choose(n, p); x++) { 
		combination(c, n, p, x);
		for (int i = 0; i < p; i++)
			space[(x-1)*p+i] = c[i] - 1;
	}
}
