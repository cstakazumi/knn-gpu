#ifndef OPENCL_H
#define OPENCL_H

#define CL_MICROCLUSTER "microcluster.cl"
#define CL_SORT "sort.cl"
#define CL_KNN "knn.cl"

/* Platform values */
#define NV_OPENCLPLATFORM 0 // NVIDIA
#define NV_OPENCLDEVICE 0 // GTX 780 Ti

#define AMD_OPENCLPLATFORM 1 // AMD
#define AMD_OPENCLDEVICE 0 // AMD R7 "Spectre"
#define CPU_OPENCLDEVICE 0 // 7850K "Kaveri"

#define NV_WORKGROUP_SIZE 1024 // NVIDIA
#define AMD_WORKGROUP_SIZE 256 // AMD
#define CPU_WORKGROUP_SIZE 1024 // CPU (AMD 7850K)

#define WG_MULTIPLIER 1 // Max wg concurrently scueduled per core, not sure if optimal
#define NV_WORKGROUP_COUNT 15 // GTX 780 Ti
#define AMD_WORKGROUP_COUNT 8 // AMD R7 "Spectre"
#define CPU_WORKGROUP_COUNT 4 // AMD 7850K "Kaveri"

#define NV_WORKGROUP_SIZE_MULT 32
#define AMD_WORKGROUP_SIZE_MULT 64

#define CL_PROFILING TIMER // Profiling is on if TIMER is on
#define ZEROCOPY 1 // 0 for PINNED memory on optimized path
#define USE_SMALL_PARTITIONS 0
#define GLOBAL_SORT 1

void opencl_sort_index(float *input[], int *index[], int n, int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

void opencl_knn(float *data[], float *distances[], int *index[], float *queries[], int n, int r, int k, int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

void opencl_dualc_knn(float *data[], float *distances[], int *index[], float *queries[], int n, int r, int k, int q, bool knnUseGPU, unsigned int knnOclDevice, unsigned int knnOclPlatform, unsigned int knnOclWgSize, unsigned int knnOclWgCount, bool sortUseGPU, unsigned int sortOclDevice, unsigned int sortOclPlatform, unsigned int sortOclWgSize, unsigned int sortOclWgCount);

void opencl_minmaxheapsort_index(float *input[], int *index[], int n, int q, int k, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

void opencl_reduce_minmaxheapsort_index(float *input[], int *index[], int n, int q, int k, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

void opencl_minmaxheapsort_max(float *input[], float *maxval[], int n, int q, int k, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

void opencl_reduce_minmaxheapsort_max(float *input[], float *maxval[], int n, int q, int k, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

void opencl_minmaxheapsort_threshold(float *input[], int *index[], float *thresholds[], int n, int q, int k, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

void opencl_distances(float *data[], float *distances[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

void opencl_cached_distances(float *data[], float *distances[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

void opencl_cached_multiq_distances(float *data[], float *distances[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

void opencl_threshold_distances(float *data[], float *distances[], float *thresholds[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

void opencl_threshold_distances(float *data[], float *distances[], float *thresholds[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

void opencl_threshold_multiq_distances(float *data[], float *distances[], float *thresholds[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

void opencl_matrix_distances(float *data[], float *distances[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount);

#endif
