#include <CL/cl.hpp>
#include <iostream>
#include <algorithm>
#include <string>
using std::string;
#include <fstream>
#include <sstream>
#include <iomanip>
#include <sys/stat.h> // to get size of file
#include <cstdlib>
#include <cmath>
#include <inttypes.h>
#include "main.h"
#include "opencl.h"
#include "timer.h"
#include "lea.h"
#include "clBLAS.h" // OpenCL BLAS libraries

/* OpenCL init routines borrowed from ranluxcltest.cpp */
/* get_CL_errstring returns the error string associated with a given OpenCL error code */
string get_CL_errstring(cl_int err) {
	switch (err) {
		case CL_SUCCESS:                          return "Success!";
		case CL_DEVICE_NOT_FOUND:                 return "Device not found";
		case CL_DEVICE_NOT_AVAILABLE:             return "Device not available";
		case CL_COMPILER_NOT_AVAILABLE:           return "Compiler not available";
		case CL_MEM_OBJECT_ALLOCATION_FAILURE:    return "Memory object allocation failure";
		case CL_OUT_OF_RESOURCES:                 return "Out of resources";
		case CL_OUT_OF_HOST_MEMORY:               return "Out of host memory";
		case CL_PROFILING_INFO_NOT_AVAILABLE:     return "Profiling information not available";
		case CL_MEM_COPY_OVERLAP:                 return "Memory copy overlap";
		case CL_IMAGE_FORMAT_MISMATCH:            return "Image format mismatch";
		case CL_IMAGE_FORMAT_NOT_SUPPORTED:       return "Image format not supported";
		case CL_BUILD_PROGRAM_FAILURE:            return "Program build failure";
		case CL_MAP_FAILURE:                      return "Map failure";
		case CL_INVALID_VALUE:                    return "Invalid value";
		case CL_INVALID_DEVICE_TYPE:              return "Invalid device type";
		case CL_INVALID_PLATFORM:                 return "Invalid platform";
		case CL_INVALID_DEVICE:                   return "Invalid device";
		case CL_INVALID_CONTEXT:                  return "Invalid context";
		case CL_INVALID_QUEUE_PROPERTIES:         return "Invalid queue properties";
		case CL_INVALID_COMMAND_QUEUE:            return "Invalid command queue";
		case CL_INVALID_HOST_PTR:                 return "Invalid host pointer";
		case CL_INVALID_MEM_OBJECT:               return "Invalid memory object";
		case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:  return "Invalid image format descriptor";
		case CL_INVALID_IMAGE_SIZE:               return "Invalid image size";
		case CL_INVALID_SAMPLER:                  return "Invalid sampler";
		case CL_INVALID_BINARY:                   return "Invalid binary";
		case CL_INVALID_BUILD_OPTIONS:            return "Invalid build options";
		case CL_INVALID_PROGRAM:                  return "Invalid program";
		case CL_INVALID_PROGRAM_EXECUTABLE:       return "Invalid program executable";
		case CL_INVALID_KERNEL_NAME:              return "Invalid kernel name";
		case CL_INVALID_KERNEL_DEFINITION:        return "Invalid kernel definition";
		case CL_INVALID_KERNEL:                   return "Invalid kernel";
		case CL_INVALID_ARG_INDEX:                return "Invalid argument index";
		case CL_INVALID_ARG_VALUE:                return "Invalid argument value";
		case CL_INVALID_ARG_SIZE:                 return "Invalid argument size";
		case CL_INVALID_KERNEL_ARGS:              return "Invalid kernel arguments";
		case CL_INVALID_WORK_DIMENSION:           return "Invalid work dimension";
		case CL_INVALID_WORK_GROUP_SIZE:          return "Invalid work group size";
		case CL_INVALID_WORK_ITEM_SIZE:           return "Invalid work item size";
		case CL_INVALID_GLOBAL_OFFSET:            return "Invalid global offset";
		case CL_INVALID_EVENT_WAIT_LIST:          return "Invalid event wait list";
		case CL_INVALID_EVENT:                    return "Invalid event";
		case CL_INVALID_OPERATION:                return "Invalid operation";
		case CL_INVALID_GL_OBJECT:                return "Invalid OpenGL object";
		case CL_INVALID_BUFFER_SIZE:              return "Invalid buffer size";
		case CL_INVALID_MIP_LEVEL:                return "Invalid mip-map level";
		case -1024:                               return "clBLAS: Functionality is not implemented";
		case -1023:                               return "clBLAS: Library is not initialized";
		case -1022:                               return "clBLAS: Matrix A is not a valid memory object";
		case -1021:                               return "clBLAS: Matrix B is not a valid memory object";
		case -1020:                               return "clBLAS: Matrix C is not a valid memory object";
		case -1019:                               return "clBLAS: Vector X is not a valid memory object";
        case -1018:                               return "clBLAS: Vector Y is not a valid memory object";
        case -1017:                               return "clBLAS: An input dimension (M,N,K) is invalid";
        case -1016:                               return "clBLAS: Leading dimension A must not be less than the size of the first dimension";
        case -1015:                               return "clBLAS: Leading dimension B must not be less than the size of the second dimension";
        case -1014:                               return "clBLAS: Leading dimension C must not be less than the size of the third dimension";
        case -1013:                               return "clBLAS: The increment for a vector X must not be 0";
        case -1012:                               return "clBLAS: The increment for a vector Y must not be 0";
        case -1011:                               return "clBLAS: The memory object for Matrix A is too small";
        case -1010:                               return "clBLAS: The memory object for Matrix B is too small";
        case -1009:                               return "clBLAS: The memory object for Matrix C is too small";
        case -1008:                               return "clBLAS: The memory object for Vector X is too small";
        case -1007:                               return "clBLAS: The memory object for Vector Y is too small";
		default:                                  return "Unknown error";
	}
}

/* check_err is a helper function to check for errors in OpenCL API calls
   and print a provided message along with the error code */
void check_err(cl_int err, const char *Message) {
	if (err != CL_SUCCESS) {
		std::cerr << "Error: (OpenCL Error " << err << ") - " << Message << " - " << get_CL_errstring(err) << std::endl;
		std::cout << "Fatal error - stop." << std::endl;
#if !BATCH
		std::cin.get();
#endif
		exit(EXIT_FAILURE);
	}
}

void compile_OpenCL_code(string BuildOptions, string FileName, string &BuildLog, cl::Context context, cl::Program &program, std::vector<cl::Device> devices, cl_int deviceNr) {
	cl_int err;

	/* Loading source code */
	std::ifstream file(FileName.c_str());
	check_err(file.is_open() ? CL_SUCCESS : CL_BUILD_PROGRAM_FAILURE, FileName.c_str());
	string prog(std::istreambuf_iterator<char>(file), (std::istreambuf_iterator<char>()));
	cl::Program::Sources source(1, std::make_pair(prog.c_str(), prog.length()+1));
	program = cl::Program(context, source);

	/* Building source code */
	err = program.build(devices, BuildOptions.c_str());
	check_err(file.is_open() ? CL_SUCCESS : CL_BUILD_PROGRAM_FAILURE, "Building OpenCL source code");
	program.getBuildInfo(devices[deviceNr], CL_PROGRAM_BUILD_LOG, &BuildLog);
}

void OpenCL_initializations(cl::Context &context, std::vector<cl::Device> &devices, cl::CommandQueue &queue, bool PrintOpenCLInfo, bool UseOpenCLProfiling, bool UseGPU, cl_uint deviceNr, cl_uint platformNr) {
	std::vector<cl::Platform> platformList;
	cl_int err;

	/* Getting list of platforms */
	cl::Platform::get(&platformList);
	check_err(platformList.size()!=0 ? CL_SUCCESS : CL_DEVICE_NOT_FOUND, "Get platform list");

	if (platformNr >= platformList.size()) {
		std::cerr << "Platform no. " << platformNr << " (indexing starts at zero) cannot be selected as there are only "
		          << platformList.size() << " platforms available" << std::endl;
		check_err(CL_INVALID_PLATFORM, "Platform number");
	}

	cl_context_properties cprops[3] = {CL_CONTEXT_PLATFORM, (cl_context_properties)(platformList[platformNr])(), 0};

	/* Creating context */
	if (UseGPU) {
		context = cl::Context(CL_DEVICE_TYPE_GPU, cprops, NULL, NULL, &err);
	} else {
		context = cl::Context(CL_DEVICE_TYPE_CPU, cprops, NULL, NULL, &err);
	}
	check_err(err, "Creating context");

	/* Getting devices from context */
	devices = context.getInfo<CL_CONTEXT_DEVICES>();
	check_err(devices.size() > 0 ? CL_SUCCESS : CL_DEVICE_NOT_FOUND, "Getting devices");

	if (deviceNr >= devices.size()) {
		std::cerr << "Device no. " << deviceNr << " (indexing starts at zero) cannot be selected as there are only "
		          << devices.size() << " devices available" << std::endl;
		check_err(CL_INVALID_DEVICE, "Device number");
	}

	/* Creating command queue */
	if (UseOpenCLProfiling) {
		queue = cl::CommandQueue(context, devices[deviceNr], CL_QUEUE_PROFILING_ENABLE, &err);
	} else {
		queue = cl::CommandQueue(context, devices[deviceNr], 0, &err);
	}
	check_err(err, "Command queue");

	//Print some OpenCL information if requested
	if (PrintOpenCLInfo) {
		cl_uint uint;
		cl_ulong ulong;
		string str;

		std::cout << "No. of platforms:       " << platformList.size() << std::endl;

		string PlatformVendor;
		platformList[platformNr].getInfo(CL_PLATFORM_VENDOR, &PlatformVendor);
		std::cout << "Platform vendor:     " << PlatformVendor << std::endl;

		platformList[platformNr].getInfo(CL_PLATFORM_PROFILE, &str);
		std::cout << "Platform profile:    " << str << std::endl;

		platformList[platformNr].getInfo(CL_PLATFORM_VERSION, &str);
		std::cout << "Platform version:    " << str << std::endl;

		platformList[platformNr].getInfo(CL_PLATFORM_NAME, &str);
		std::cout << "Platform name:       " << str << std::endl;

		platformList[platformNr].getInfo(CL_PLATFORM_EXTENSIONS, &str);
		std::cout << "Platform extensions: " << str << std::endl;

		devices[deviceNr].getInfo(CL_DEVICE_NAME, &str);
		std::cout << "Device name:         " << str << std::endl;

		cl_device_type devicetype;
		devices[deviceNr].getInfo(CL_DEVICE_TYPE, &devicetype);
		std::cout << "Device type:         ";
		if (devicetype == CL_DEVICE_TYPE_CPU) std::cout << "CPU ";
		if (devicetype == CL_DEVICE_TYPE_GPU) std::cout << "GPU ";
		if (devicetype == CL_DEVICE_TYPE_ACCELERATOR) std::cout << "Accelerator ";
		if (devicetype == CL_DEVICE_TYPE_DEFAULT) std::cout << "Default ";
		std::cout << std::endl;

		devices[deviceNr].getInfo(CL_DEVICE_VENDOR_ID, &uint);
		std::cout << "Device vendor ID:    " << uint << std::endl;

		devices[deviceNr].getInfo(CL_DEVICE_MAX_COMPUTE_UNITS, &uint);
		std::cout << "Max compute units:   " << uint << std::endl;

		devices[deviceNr].getInfo(CL_DEVICE_MAX_WORK_GROUP_SIZE, &ulong);
		std::cout << "Max work group size: " << ulong << std::endl;

		devices[deviceNr].getInfo(CL_DEVICE_MAX_MEM_ALLOC_SIZE, &ulong);
		std::cout << "Max mem alloc size:  " << ulong/(1024*1024.0) << " MiB\n";

		devices[deviceNr].getInfo(CL_DEVICE_GLOBAL_MEM_SIZE, &ulong);
		std::cout << "Global memory size:  " << ulong/(1024*1024.0) << " MiB\n";

		devices[deviceNr].getInfo(CL_DEVICE_GLOBAL_MEM_CACHE_TYPE, &ulong);
		std::cout << "Global memory cache: ";
		if (ulong == CL_NONE) std::cout << "None\n";
		if (ulong == CL_READ_ONLY_CACHE) std::cout << "Read only\n";
		if (ulong == CL_READ_WRITE_CACHE) std::cout << "Read/Write cache\n";

		devices[deviceNr].getInfo(CL_DEVICE_LOCAL_MEM_SIZE, &ulong);
		std::cout << "Local memory size:   " << ulong/1024.0 << " KiB\n";

		devices[deviceNr].getInfo(CL_DEVICE_LOCAL_MEM_TYPE, &ulong);
		std::cout << "Local memory type:   ";
		if (ulong == CL_LOCAL) std::cout << "Dedicated\n";
		if (ulong == CL_GLOBAL) std::cout << "Global memory\n";

		devices[deviceNr].getInfo(CL_DEVICE_MAX_CONSTANT_ARGS, &uint);
		std::cout << "Max constant args:   " << uint << std::endl;

		devices[deviceNr].getInfo(CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, &ulong);
		std::cout << "Const buffer size:   " << ulong/1024.0 << " KiB\n";

		devices[deviceNr].getInfo(CL_DEVICE_PROFILING_TIMER_RESOLUTION, &ulong);
		std::cout << "Prof. timer res:     " << ulong << " ns\n";

		devices[deviceNr].getInfo(CL_DRIVER_VERSION, &str);
		std::cout << "OpenCL driver ver:   " << str << std::endl;
	}
}
/* End of init code */


/* Local insertion sort only */
void opencl_sort_index(float *input[], int *index[], int n, int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	/* More init code borrowed from ranluxcltest.cpp */
	/* OpenCL variables */
	cl::Context context;
	std::vector <cl::Device> devices;
	cl::CommandQueue queue;
	cl::Program program;

	OpenCL_initializations(context, devices, queue, DEBUG, CL_PROFILING, useGPU, openclDevice, openclPlatform);

	/* Building OpenCL program */
	std::string BuildOptions;
	std::string BuildLog;
	std::string FileName = CL_SORT;
	BuildOptions += "-cl-fast-relaxed-math -I . "; // Search for include files in current directory
	compile_OpenCL_code(BuildOptions, FileName, BuildLog, context, program, devices, openclDevice);

	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	cl_int err;
	int mwgsize = min(n, workgroupSize);
	int mwgcount = ceil((float)n / (float)mwgsize);
	int mpadding = (mwgcount*mwgsize) - n;

	/* Setting kernels */
	cl::Kernel parallel_sort_index = cl::Kernel(program, "parallel_selection_sort_index", &err); check_err(err, "Kernel parallel_selection_sort_index");

	/* Setup for sort_index */
	cl::Buffer inputBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*n*q, *input, &err); check_err(err, "Init inputBuffer");
	cl::Buffer indexBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, (int)sizeof(cl_int)*n*q, NULL, &err); check_err(err, "Init indexBuffer");

	err = parallel_sort_index.setArg(0, inputBuffer); check_err(err, "Set kernel parallel_selection_sort_index arg inputBuffer");
	err = parallel_sort_index.setArg(1, indexBuffer); check_err(err, "Set kernel parallel_selection_sort_index arg indexBuffer");
	err = parallel_sort_index.setArg(2, n); check_err(err, "Set kernel parallel_selection_sort_index arg n");
	err = parallel_sort_index.setArg(3, q); check_err(err, "Set kernel parallel_selection_sort_index arg q");
	err = parallel_sort_index.setArg(4, 0); check_err(err, "Set kernel parallel_selection_sort_index arg offset");
	err = parallel_sort_index.setArg(5, cl::__local(n*sizeof(cl_float))); check_err(err, "Set kernel parallel_selection_sort_index argument heapData (local)");
	err = parallel_sort_index.setArg(6, cl::__local(n*sizeof(cl_uint))); check_err(err, "Set kernel parallel_selection_sort_index argument heapIdx (local)");

#if TIMER
	time_t ltime;
	time(&ltime);
	double timeElapsed = 0.0;
	Timer *t;
	t = new Timer();
#endif
	/* Do work: sorting */
	err = queue.enqueueNDRangeKernel(parallel_sort_index, cl::NullRange, cl::NDRange(n*q), cl::NDRange(n), 0, 0); check_err(err, "Enqueue parallel_sort_index kernel");

	//err = queue.finish(); check_err(err, "Clear work queue");

	/* Download results from device */
	err = queue.enqueueReadBuffer(indexBuffer, CL_TRUE, 0, sizeof(cl_int)*n*q, *index, 0, 0); check_err(err, "Copy indexBuffer from device"); // only first k elements are valid
#if TIMER
	timeElapsed = t->elapsed();
	std::cout << "OpenCL parallel_selection_sort_index time: " << timeElapsed << "s" << std::endl;
	delete t;
#endif
	/* Done with buffers */
	inputBuffer = cl::Buffer();
	indexBuffer = cl::Buffer();
}



/* Full KNN + sort */
void opencl_knn(float *data[], float *distances[], int *index[], float *queries[], int n, int r, int k, int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	/* OpenCL variables */
	cl::Context context;
	std::vector <cl::Device> devices;
	cl::CommandQueue queue;
	cl::Program cl_knn_program, cl_sort_program;
	cl::Event ocltimer_knn, ocltimer_sort;
	cl_int err;

	OpenCL_initializations(context, devices, queue, DEBUG, CL_PROFILING, useGPU, openclDevice, openclPlatform); // Slow

	/* Building OpenCL program */
	std::string BuildOptions = "-DPARRAYSIZE=" + std::to_string(k) + " -cl-fast-relaxed-math -I . "; // Search for include files in current directory
	std::string BuildLog;
	compile_OpenCL_code(BuildOptions, CL_KNN, BuildLog, context, cl_knn_program, devices, openclDevice);
	compile_OpenCL_code(BuildOptions, CL_SORT, BuildLog, context, cl_sort_program, devices, openclDevice);

	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	unsigned int knn_wgobjects = 1;
	unsigned int knn_padding = 0;

	if (openclPlatform == NV_OPENCLPLATFORM) { // Nvidia, warp size 32, local mem 48KB
		if (q == 1 && r <= 12 && n >= 1024) knn_wgobjects = 1024;
		else if (q <= 2 && r <= 24 && n >= 512) knn_wgobjects = 512;
		else if (q <= 4 && r <= 48 && n >= 256) knn_wgobjects = 256;
		else if (q <= 8 && r <= 96 && n >= 128) knn_wgobjects = 128;
		else if (q <= 16 && r <= 192 && n >= 64) knn_wgobjects = 64;
		else if (q <= 32 && r <= 384 && n >= 32) knn_wgobjects = 32;
#if USE_SMALL_PARTITIONS
		else if (q <= 64 && r <= 768 && n >= 16) knn_wgobjects = 16;
		else if (q <= 128 && r <= 1536 && n >= 8) knn_wgobjects = 8;
		else if (q <= 256 && r <= 3072 && n >= 4) knn_wgobjects = 4;
		else if (q <= 512 && r <= 6144 && n >= 2) knn_wgobjects = 2;
#endif
	} else if (openclPlatform == AMD_OPENCLPLATFORM) { // AMD, warp (wavefront) size 64, local mem 32KB
		if (!useGPU && q == 1 && r <= 8 && n >= 1024) knn_wgobjects = 1024;
		else if (!useGPU && q <= 2 && r <= 16 && n >= 512) knn_wgobjects = 512;
		else if (((!useGPU && q <= 4) || q <= 1) && r <= 32 && n >= 256) knn_wgobjects = 256; // AMD GPUs max workgroup size 256
		else if (((!useGPU && q <= 8) || q <= 2) && r <= 64 && n >= 128) knn_wgobjects = 128;
		else if (((!useGPU && q <= 16) || q <= 4) && r <= 128 && n >= 64) knn_wgobjects = 64;
#if USE_SMALL_PARTITIONS
		else if (((!useGPU && q <= 32) || q <= 8) && r <= 256 && n >= 32) knn_wgobjects = 32;
		else if (((!useGPU && q <= 64) || q <= 16) && r <= 512 && n >= 16) knn_wgobjects = 16;
		else if (((!useGPU && q <= 128) || q <= 32) && r <= 1024 && n >= 8) knn_wgobjects = 8;
		else if (((!useGPU && q <= 256) || q <= 64) && r <= 2048 && n >= 4) knn_wgobjects = 4;
		else if (((!useGPU && q <= 512) || q <= 128) && r <= 4096 && n >= 2) knn_wgobjects = 2;
#endif
	}

	if (knn_wgobjects > 1) knn_padding = (q*knn_wgobjects) - ((n*q)%(q*knn_wgobjects));

	DBG(std::cout << "opencl_knn: knn_wgobjects = " << knn_wgobjects << " knn_padding = " << knn_padding << std::endl;);

	unsigned int sort_partitions = 1;
	if (openclPlatform == NV_OPENCLPLATFORM) { // Nvidia, warp size 32, local mem 48KB
		if (k < 6 && n >= 1024*k) sort_partitions = 1024;
		else if (k < 12 && n >= 512*k) sort_partitions = 512;
		else if (k < 24 && n >= 256*k) sort_partitions = 256;
		else if (k < 48 && n >= 128*k) sort_partitions = 128;
		else if (k < 96 && n >= 64*k) sort_partitions = 64;
		else if (k < 192 && n >= 32*k) sort_partitions = 32;
#if USE_SMALL_PARTITIONS
		else if (k < 384 && n >= 16*k) sort_partitions = 16;
		else if (k < 768 && n >= 8*k) sort_partitions = 8;
		else if (k < 1536 && n >= 4*k) sort_partitions = 4;
		else if (k < 3072 && n >= 2*k) sort_partitions = 2;
#endif
	} else if (openclPlatform == AMD_OPENCLPLATFORM) { // AMD, warp (wavefront) size 64, local mem 32KB
		if (!useGPU && k < 4 && n >= 1024*k) sort_partitions = 1024;
		else if (!useGPU && k < 8 && n >= 512*k) sort_partitions = 512;
		else if (k < 16 && n >= 256*k) sort_partitions = 256; // AMD GPUs max workgroup size 256
		else if (k < 32 && n >= 128*k) sort_partitions = 128;
		else if (k < 64 && n >= 64*k) sort_partitions = 64;
#if USE_SMALL_PARTITIONS
		else if (k < 128 && n >= 32*k) sort_partitions = 32;
		else if (k < 256 && n >= 16*k) sort_partitions = 16;
		else if (k < 512 && n >= 8*k) sort_partitions = 8;
		else if (k < 1024 && n >= 4*k) sort_partitions = 4;
		else if (k < 2048 && n >= 2*k) sort_partitions = 2;
#endif
	}

	DBG(std::cout << "opencl_knn: sort_partitions = " << sort_partitions << std::endl;);

	/* Setting kernels */
	cl::Kernel parallel_distances;
	cl::Kernel parallel_sort_index;
	if (knn_wgobjects == 1){
		parallel_distances = cl::Kernel(cl_knn_program, "parallel_distances_cached_worker", &err); check_err(err, "Kernel parallel_distances_worker");
	} else {
		parallel_distances = cl::Kernel(cl_knn_program, "parallel_distances_cached_multi_worker", &err); check_err(err, "Kernel parallel_distances_cached_multi_worker");
	}
#if GLOBAL_SORT
	sort_partitions = 1;
	parallel_sort_index = cl::Kernel(cl_sort_program, "parallel_global_minmaxheapsort_index", &err); check_err(err, "Kernel parallel_global_minmaxheapsort_index");
#else
	if (sort_partitions == 1) {
		parallel_sort_index = cl::Kernel(cl_sort_program, "parallel_minmaxheapsort_index", &err); check_err(err, "Kernel parallel_minmaxheapsort_index");
	} else {
		parallel_sort_index = cl::Kernel(cl_sort_program, "parallel_reduce_minmaxheapsort_index", &err); check_err(err, "Kernel parallel_reduce_minmaxheapsort_index");
	}
#endif

	/* Setup for parallel_distances */
#if ZEROCOPY
	cl::Buffer dataBufferPinned = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (int)sizeof(cl_float)*n*r, *data, &err); check_err(err, "Init dataBufferPinned");
	cl::Buffer queriesBufferPinned = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (int)sizeof(cl_float)*q*r, *queries, &err); check_err(err, "Init queriesBufferPinned");
	cl::Buffer distancesBufferPinned = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, (int)sizeof(cl_float)*n*q, *distances, &err); check_err(err, "Init distanceBufferPinned");
#else
	/* Pinned host memory */
	cl::Buffer dataBufferPinned = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, (int)sizeof(cl_float)*n*r, nullptr, &err); check_err(err, "Init dataBufferPinned");
	cl::Buffer queriesBufferPinned = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, (int)sizeof(cl_float)*q*r, nullptr, &err); check_err(err, "Init queriesBufferPinned");
	cl::Buffer distancesBufferPinned = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, (int)sizeof(cl_float)*n*q, nullptr, &err); check_err(err, "Init distanceBufferPinned");
	float *dataBufferHost = (float *)queue.enqueueMapBuffer(dataBufferPinned, CL_FALSE, CL_MAP_READ, 0, (int)sizeof(cl_float)*n*r, 0, 0, &err); check_err(err, "Map dataBufferPinned");
	float *queriesBufferHost = (float *)queue.enqueueMapBuffer(queriesBufferPinned, CL_FALSE, CL_MAP_READ, 0, (int)sizeof(cl_float)*q*r, 0, 0, &err); check_err(err, "Map queriesBufferPinned");
	float *distancesBufferHost = (float *)queue.enqueueMapBuffer(distancesBufferPinned, CL_TRUE, CL_MAP_WRITE, 0, (int)sizeof(cl_float)*n*q, 0, 0, &err); check_err(err, "Map distancesBufferPinned");

	memcpy(dataBufferHost, *data, sizeof(cl_float)*n*r);
	memcpy(queriesBufferHost, *queries, sizeof(cl_float)*q*r);

	/* Device memory */
	cl::Buffer dataBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, (int)sizeof(cl_float)*n*r, nullptr, &err); check_err(err, "Init dataBuffer");
	cl::Buffer queriesBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, (int)sizeof(cl_float)*q*r, nullptr, &err); check_err(err, "Init queriesBuffer");
	cl::Buffer distancesBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_float)*n*q, nullptr, &err); check_err(err, "Init distanceBuffer");
	err = queue.enqueueWriteBuffer(dataBuffer, CL_FALSE, 0, (int)sizeof(cl_float)*n*r, dataBufferHost, 0, 0); check_err(err, "Enqueue copy dataBuffer");
	err = queue.enqueueWriteBuffer(queriesBuffer, CL_FALSE, 0, (int)sizeof(cl_float)*q*r, queriesBufferHost, 0, 0); check_err(err, "Enqueue copy queriesBuffer");
#endif
#if ZEROCOPY
	err = parallel_distances.setArg(0, dataBufferPinned); check_err(err, "Set kernel parallel_distances_worker arg dataBufferPinned");
	err = parallel_distances.setArg(1, distancesBufferPinned); check_err(err, "Set kernel parallel_distances_worker arg distancesBufferPinned");
	err = parallel_distances.setArg(4, queriesBufferPinned); check_err(err, "Set kernel parallel_distances_worker arg queriesBufferPinned");
#else
	err = parallel_distances.setArg(0, dataBuffer); check_err(err, "Set kernel parallel_distances_worker arg dataBuffer");
	err = parallel_distances.setArg(1, distancesBuffer); check_err(err, "Set kernel parallel_distances_worker arg distancesBuffer");
	err = parallel_distances.setArg(4, queriesBuffer); check_err(err, "Set kernel parallel_distances_worker arg queriesBuffer");
#endif
	err = parallel_distances.setArg(2, n); check_err(err, "Set kernel parallel_distances_worker arg n");
	err = parallel_distances.setArg(3, r); check_err(err, "Set kernel parallel_distances_worker arg r");
	err = parallel_distances.setArg(5, q); check_err(err, "Set kernel parallel_distances_worker arg q");
	err = parallel_distances.setArg(6, cl::__local(knn_wgobjects*r*sizeof(cl_float))); check_err(err, "Set kernel parallel_distances_worker argument ldata (local)");
	DBG(std::cout << "    Dist buf:  " << (sizeof(float)*knn_wgobjects*r)/1024 << "KB (" << knn_wgobjects << " x " << r << ")" << std::endl;);

	/* Setup for sort_index */
#if ZEROCOPY
	cl::Buffer indexBufferPinned = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, (int)sizeof(cl_int)*k*q, *index, &err); check_err(err, "Init indexBufferPinned");
	err = parallel_sort_index.setArg(0, distancesBufferPinned); check_err(err, "Set kernel parallel_minmaxheapsort_index arg distancesBufferPinned");
	err = parallel_sort_index.setArg(1, indexBufferPinned); check_err(err, "Set kernel parallel_minmaxheapsort_index arg indexBufferPinned");
#else
	/* Pinned host memory */
	cl::Buffer indexBufferPinned = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, (int)sizeof(cl_int)*k*q, nullptr, &err); check_err(err, "Init indexBufferPinned");
	float *indexBufferHost = (float *)queue.enqueueMapBuffer(indexBufferPinned, CL_FALSE, CL_MAP_WRITE, 0, (int)sizeof(cl_int)*k*q, 0, 0, &err); check_err(err, "Map indexBufferPinned");
	cl::Buffer indexBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, (int)sizeof(cl_int)*k*q, NULL, &err); check_err(err, "Init indexBuffer");

	err = parallel_sort_index.setArg(0, distancesBuffer); check_err(err, "Set kernel parallel_minmaxheapsort_index arg distancesBuffer");
	err = parallel_sort_index.setArg(1, indexBuffer); check_err(err, "Set kernel parallel_minmaxheapsort_index arg indexBuffer");
#endif
	err = parallel_sort_index.setArg(2, n); check_err(err, "Set kernel parallel_minmaxheapsort_index arg n");
	err = parallel_sort_index.setArg(3, q); check_err(err, "Set kernel parallel_minmaxheapsort_index arg q");
	err = parallel_sort_index.setArg(4, k); check_err(err, "Set kernel parallel_minmaxheapsort_index arg k");
#if !GLOBAL_SORT
	err = parallel_sort_index.setArg(5, cl::__local(k*sizeof(cl_float)*sort_partitions)); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapData (local)");
	err = parallel_sort_index.setArg(6, cl::__local(k*sizeof(cl_uint)*sort_partitions)); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapIdx (local)");
	DBG(std::cout << "    HeapD buf: " << (sizeof(float)*sort_partitions*k)/1024 << "KB (" << sort_partitions << " x " << k << ")" << std::endl;);
	DBG(std::cout << "    HeapI buf: " << (sizeof(cl_uint)*sort_partitions*k)/1024 << "KB (" << sort_partitions << " x " << k << ")" << std::endl;);
#else
	cl::Buffer heapDataBufferG = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_float)*k*q, NULL, &err); check_err(err, "Init heapDataBuffer");
	cl::Buffer heapIndexBufferG = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_int)*k*q, NULL, &err); check_err(err, "Init heapIndexBuffer");
	err = parallel_sort_index.setArg(5, heapDataBufferG); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapData (local)");
	err = parallel_sort_index.setArg(6, heapIndexBufferG); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapIdx (local)");
#endif

#if TIMER
	cl_ulong start = 0, end = 0;
	cl_double total = 0.0;

	time_t ltime;
	time(&ltime);
	double timeElapsed = 0.0;
	Timer *t;
	t = new Timer();
#endif
	/* Do work */
	int offset = 0;
	int workleft = q;
	int worksize; 
	do {
		worksize = min(workleft, workgroupSize);
		if (knn_wgobjects == 1) err = parallel_distances.setArg(7, offset); check_err(err, "Set kernel parallel_distances_worker arg offset"); // Used if q > wg
		err = queue.enqueueNDRangeKernel(parallel_distances, cl::NullRange, cl::NDRange(n*worksize+knn_padding), cl::NDRange(worksize*knn_wgobjects), 0, &ocltimer_knn); check_err(err, "Enqueue parallel_distances kernel");
		offset += worksize;
		workleft -= worksize;
#if CL_PROFILING
		err = queue.finish(); check_err(err, "Clear work queue");
		ocltimer_knn.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
		ocltimer_knn.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
		total += (cl_double)(end - start)*(cl_double)(1e-09);
#endif
		DBG(std::cout << "opencl_knn: worksize = " << worksize << " workleft = " << workleft << std::endl;);
	} while (workleft > 0);

#if !ZEROCOPY && DUMPOUTPUT
	err = queue.enqueueReadBuffer(distancesBuffer, CL_FALSE, 0, sizeof(cl_float)*n*q, distancesBufferHost, 0, 0); check_err(err, "Copy distancesBuffer from device");
#endif
#if !GLOBAL_SORT
	err = queue.enqueueNDRangeKernel(parallel_sort_index, cl::NullRange, cl::NDRange(q*sort_partitions), cl::NDRange(sort_partitions), 0, &ocltimer_sort); check_err(err, "Enqueue parallel_sort_index kernel");
	DBG(std::cout << "    Sort kern: " << q*sort_partitions << " x " << sort_partitions << std::endl;);
#else
	err = queue.enqueueNDRangeKernel(parallel_sort_index, cl::NullRange, cl::NDRange(q), cl::NDRange(min(q,workgroupSize)), 0, &ocltimer_sort); check_err(err, "Enqueue parallel_sort_index kernel");
#endif

#if !ZEROCOPY
	err = queue.enqueueReadBuffer(indexBuffer, CL_FALSE, 0, sizeof(cl_int)*k*q, indexBufferHost, 0, 0); check_err(err, "Copy indexBuffer from device");
#else
	int *indexBufferHost = (int *)queue.enqueueMapBuffer(indexBufferPinned, CL_FALSE, CL_MAP_READ, 0, (int)sizeof(cl_int)*k*q, 0, 0, &err); check_err(err, "Map indexBufferPinned");
#if DUMPOUTPUT
	float *distancesBufferHost = (float *)queue.enqueueMapBuffer(distancesBufferPinned, CL_FALSE, CL_MAP_READ, 0, (int)sizeof(cl_float)*n*q, 0, 0, &err); check_err(err, "Map distancesBufferPinned");
#endif
#endif
	err = queue.finish(); check_err(err, "Clear work queue");

#if !ZEROCOPY
	/* Copy back into original arrays */
	memcpy(*index, indexBufferHost, sizeof(cl_uint)*k*q);
#if DUMPOUTPUT
	memcpy(*distances, distancesBufferHost, sizeof(cl_float)*n*q);
#endif
#endif

#if TIMER
	timeElapsed = t->elapsed();
#endif

#if CL_PROFILING
	std::cout << "OpenCL Device distance time: " << total << "s" << std::endl;
	std::cout << "           distance iterated " << (q/workgroupSize) + 1 << " times" << std::endl;
	start = 0, end = 0;
	ocltimer_sort.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
	ocltimer_sort.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
	std::cout << "OpenCL Device sort time: " << (cl_double)(end - start)*(cl_double)(1e-09) << "s" << std::endl;
#endif

#if TIMER
	std::cout << "OpenCL Host KNN time: " << timeElapsed << "s" << std::endl;
	delete t;
#endif
}


/* Dual context */
void opencl_dualc_knn(float *data[], float *distances[], int *index[], float *queries[], int n, int r, int k, int q, bool knnUseGPU, unsigned int knnOclDevice, unsigned int knnOclPlatform, unsigned int knnOclWgSize, unsigned int knnOclWgCount, bool sortUseGPU, unsigned int sortOclDevice, unsigned int sortOclPlatform, unsigned int sortOclWgSize, unsigned int sortOclWgCount) {
	/* OpenCL variables */
	cl::Context knnOclContext, sortOclContext;
	std::vector <cl::Device> knnOclDevices, sortOclDevices;
	cl::CommandQueue knnOclQueue, sortOclQueue;
	cl::Program cl_knn_program, cl_sort_program;
	cl::Event ocltimer_knn, ocltimer_sort, ocltimer_xfer;
	cl_int err;

	OpenCL_initializations(knnOclContext, knnOclDevices, knnOclQueue, DEBUG, CL_PROFILING, knnUseGPU, knnOclDevice, knnOclPlatform); // Slow
	OpenCL_initializations(sortOclContext, sortOclDevices, sortOclQueue, DEBUG, CL_PROFILING, sortUseGPU, sortOclDevice, sortOclPlatform); // Slow

	/* Building OpenCL program */
	std::string BuildOptions = "-DPARRAYSIZE=" + std::to_string(k) + " -cl-fast-relaxed-math -I . "; // Search for include files in current directory
	std::string BuildLog;
	compile_OpenCL_code(BuildOptions, CL_KNN, BuildLog, knnOclContext, cl_knn_program, knnOclDevices, knnOclDevice);
	compile_OpenCL_code(BuildOptions, CL_SORT, BuildLog, sortOclContext, cl_sort_program, sortOclDevices, sortOclDevice);

	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	/* Determine number of KNN workgroup objects */
	unsigned int knn_wgobjects = 1;
	unsigned int knn_padding = 0;

	if (knnOclPlatform == NV_OPENCLPLATFORM) { // Nvidia, warp size 32, local mem 48KB
		if (q == 1 && r <= 12 && n >= 1024) knn_wgobjects = 1024;
		else if (q <= 2 && r <= 24 && n >= 512) knn_wgobjects = 512;
		else if (q <= 4 && r <= 48 && n >= 256) knn_wgobjects = 256;
		else if (q <= 8 && r <= 96 && n >= 128) knn_wgobjects = 128;
		else if (q <= 16 && r <= 192 && n >= 64) knn_wgobjects = 64;
		else if (q <= 32 && r <= 384 && n >= 32) knn_wgobjects = 32;
#if USE_SMALL_PARTITIONS
		else if (q <= 64 && r <= 768 && n >= 16) knn_wgobjects = 16;
		else if (q <= 128 && r <= 1536 && n >= 8) knn_wgobjects = 8;
		else if (q <= 256 && r <= 3072 && n >= 4) knn_wgobjects = 4;
		else if (q <= 512 && r <= 6144 && n >= 2) knn_wgobjects = 2;
#endif
	} else if (knnOclPlatform == AMD_OPENCLPLATFORM) { // AMD, warp (wavefront) size 64, local mem 32KB
		if (!knnUseGPU && q == 1 && r <= 8 && n >= 1024) knn_wgobjects = 1024;
		else if (!knnUseGPU && q <= 2 && r <= 16 && n >= 512) knn_wgobjects = 512;
		else if (((!knnUseGPU && q <= 4) || q <= 1) && r <= 32 && n >= 256) knn_wgobjects = 256; // AMD GPUs max workgroup size 256
		else if (((!knnUseGPU && q <= 8) || q <= 2) && r <= 64 && n >= 128) knn_wgobjects = 128;
		else if (((!knnUseGPU && q <= 16) || q <= 4) && r <= 128 && n >= 64) knn_wgobjects = 64;
#if USE_SMALL_PARTITIONS
		else if (((!knnUseGPU && q <= 32) || q <= 8) && r <= 256 && n >= 32) knn_wgobjects = 32;
		else if (((!knnUseGPU && q <= 64) || q <= 16) && r <= 512 && n >= 16) knn_wgobjects = 16;
		else if (((!knnUseGPU && q <= 128) || q <= 32) && r <= 1024 && n >= 8) knn_wgobjects = 8;
		else if (((!knnUseGPU && q <= 256) || q <= 64) && r <= 2048 && n >= 4) knn_wgobjects = 4;
		else if (((!knnUseGPU && q <= 512) || q <= 128) && r <= 4096 && n >= 2) knn_wgobjects = 2;
#endif
	}

	if (knn_wgobjects > 1) knn_padding = (q*knn_wgobjects) - ((n*q)%(q*knn_wgobjects));

	DBG(std::cout << "opencl_knn: knn_wgobjects = " << knn_wgobjects << " knn_padding = " << knn_padding << std::endl;);

	/* Determine number of sort partitions */
	unsigned int sort_partitions = 1;
	if (sortOclPlatform == NV_OPENCLPLATFORM) { // Nvidia, warp size 32, local mem 48KB
		if (k < 6 && n >= 1024*k) sort_partitions = 1024;
		else if (k < 12 && n >= 512*k) sort_partitions = 512;
		else if (k < 24 && n >= 256*k) sort_partitions = 256;
		else if (k < 48 && n >= 128*k) sort_partitions = 128;
		else if (k < 96 && n >= 64*k) sort_partitions = 64;
		else if (k < 192 && n >= 32*k) sort_partitions = 32;
#if USE_SMALL_PARTITIONS
		else if (k < 384 && n >= 16*k) sort_partitions = 16;
		else if (k < 768 && n >= 8*k) sort_partitions = 8;
		else if (k < 1536 && n >= 4*k) sort_partitions = 4;
		else if (k < 3072 && n >= 2*k) sort_partitions = 2;
#endif
	} else if (sortOclPlatform == AMD_OPENCLPLATFORM) { // AMD, warp (wavefront) size 64, local mem 32KB
		if (!sortUseGPU && k < 4 && n >= 1024*k) sort_partitions = 1024;
		else if (!sortUseGPU && k < 8 && n >= 512*k) sort_partitions = 512;
		else if (k < 16 && n >= 256*k) sort_partitions = 256; // AMD GPUs max workgroup size 256
		else if (k < 32 && n >= 128*k) sort_partitions = 128;
		else if (k < 64 && n >= 64*k) sort_partitions = 64;
#if USE_SMALL_PARTITIONS
		else if (k < 128 && n >= 32*k) sort_partitions = 32;
		else if (k < 256 && n >= 16*k) sort_partitions = 16;
		else if (k < 512 && n >= 8*k) sort_partitions = 8;
		else if (k < 1024 && n >= 4*k) sort_partitions = 4;
		else if (k < 2048 && n >= 2*k) sort_partitions = 2;
#endif
	}

	DBG(std::cout << "opencl_knn: sort_partitions = " << sort_partitions << std::endl;);

	/* Setting kernels */
	cl::Kernel parallel_distances;
	cl::Kernel parallel_sort_index;
	if (knn_wgobjects == 1){
		parallel_distances = cl::Kernel(cl_knn_program, "parallel_distances_vector4_worker", &err); check_err(err, "Kernel parallel_distances_vector4_worker");
	} else {
		parallel_distances = cl::Kernel(cl_knn_program, "parallel_distances_cached_multi_worker", &err); check_err(err, "Kernel parallel_distances_cached_multi_worker");
	}
#if GLOBAL_SORT
	sort_partitions = 1;
	parallel_sort_index = cl::Kernel(cl_sort_program, "parallel_global_minmaxheapsort_index", &err); check_err(err, "Kernel parallel_global_minmaxheapsort_index");
#else
	if (sort_partitions == 1) {
		parallel_sort_index = cl::Kernel(cl_sort_program, "parallel_minmaxheapsort_index", &err); check_err(err, "Kernel parallel_minmaxheapsort_index");
	} else {
		parallel_sort_index = cl::Kernel(cl_sort_program, "parallel_reduce_minmaxheapsort_index", &err); check_err(err, "Kernel parallel_reduce_minmaxheapsort_index");
	}
#endif

	/* Setup for parallel_distances */
#if ZEROCOPY
	cl::Buffer dataBufferPinned = cl::Buffer(knnOclContext, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (int)sizeof(cl_float)*n*r, *data, &err); check_err(err, "Init dataBufferPinned");
	cl::Buffer queriesBufferPinned = cl::Buffer(knnOclContext, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (int)sizeof(cl_float)*q*r, *queries, &err); check_err(err, "Init queriesBufferPinned");
	cl::Buffer distancesBufferPinned = cl::Buffer(knnOclContext, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, (int)sizeof(cl_float)*n*q, *distances, &err); check_err(err, "Init distanceBufferPinned");
#else
	/* Pinned host memory */
	cl::Buffer dataBufferPinned = cl::Buffer(knnOclContext, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, (int)sizeof(cl_float)*n*r, nullptr, &err); check_err(err, "Init dataBufferPinned");
	cl::Buffer queriesBufferPinned = cl::Buffer(knnOclContext, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, (int)sizeof(cl_float)*q*r, nullptr, &err); check_err(err, "Init queriesBufferPinned");
	cl::Buffer distancesBufferPinned = cl::Buffer(knnOclContext, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, (int)sizeof(cl_float)*n*q, nullptr, &err); check_err(err, "Init distanceBufferPinned");
	float *dataBufferHost = (float *)knnOclQueue.enqueueMapBuffer(dataBufferPinned, CL_FALSE, CL_MAP_READ, 0, (int)sizeof(cl_float)*n*r, 0, 0, &err); check_err(err, "Map dataBufferPinned");
	float *queriesBufferHost = (float *)knnOclQueue.enqueueMapBuffer(queriesBufferPinned, CL_FALSE, CL_MAP_READ, 0, (int)sizeof(cl_float)*q*r, 0, 0, &err); check_err(err, "Map queriesBufferPinned");
	float *distancesBufferHost = (float *)knnOclQueue.enqueueMapBuffer(distancesBufferPinned, CL_TRUE, CL_MAP_WRITE, 0, (int)sizeof(cl_float)*n*q, 0, 0, &err); check_err(err, "Map distancesBufferPinned");

	memcpy(dataBufferHost, *data, sizeof(cl_float)*n*r);
	memcpy(queriesBufferHost, *queries, sizeof(cl_float)*q*r);

	/* Device memory */
	cl::Buffer dataBuffer = cl::Buffer(knnOclContext, CL_MEM_READ_ONLY, (int)sizeof(cl_float)*n*r, nullptr, &err); check_err(err, "Init dataBuffer");
	cl::Buffer queriesBuffer = cl::Buffer(knnOclContext, CL_MEM_READ_ONLY, (int)sizeof(cl_float)*q*r, nullptr, &err); check_err(err, "Init queriesBuffer");
	cl::Buffer distancesBuffer = cl::Buffer(knnOclContext, CL_MEM_WRITE_ONLY, (int)sizeof(cl_float)*n*q, nullptr, &err); check_err(err, "Init distanceBuffer");
	err = knnOclQueue.enqueueWriteBuffer(dataBuffer, CL_FALSE, 0, (int)sizeof(cl_float)*n*r, dataBufferHost, 0, 0); check_err(err, "Enqueue copy dataBuffer");
	err = knnOclQueue.enqueueWriteBuffer(queriesBuffer, CL_FALSE, 0, (int)sizeof(cl_float)*q*r, queriesBufferHost, 0, 0); check_err(err, "Enqueue copy queriesBuffer");
#endif
#if ZEROCOPY
	err = parallel_distances.setArg(0, dataBufferPinned); check_err(err, "Set kernel parallel_distances_worker arg dataBufferPinned");
	err = parallel_distances.setArg(1, distancesBufferPinned); check_err(err, "Set kernel parallel_distances_worker arg distancesBufferPinned");
	err = parallel_distances.setArg(4, queriesBufferPinned); check_err(err, "Set kernel parallel_distances_worker arg queriesBufferPinned");
#else
	err = parallel_distances.setArg(0, dataBuffer); check_err(err, "Set kernel parallel_distances_worker arg dataBuffer");
	err = parallel_distances.setArg(1, distancesBuffer); check_err(err, "Set kernel parallel_distances_worker arg distancesBuffer");
	err = parallel_distances.setArg(4, queriesBuffer); check_err(err, "Set kernel parallel_distances_worker arg queriesBuffer");
#endif
	err = parallel_distances.setArg(2, n); check_err(err, "Set kernel parallel_distances_worker arg n");
	err = parallel_distances.setArg(3, r); check_err(err, "Set kernel parallel_distances_worker arg r");
	err = parallel_distances.setArg(5, q); check_err(err, "Set kernel parallel_distances_worker arg q");
	err = parallel_distances.setArg(6, cl::__local(knn_wgobjects*r*sizeof(cl_float))); check_err(err, "Set kernel parallel_distances_worker argument ldata (local)");
	DBG(std::cout << "     Dist buf: " << (sizeof(float)*knn_wgobjects*r)/1024 << "KB (" << knn_wgobjects << " x " << r << ")" << std::endl;);
	DBG(std::cout << "    HeapD buf: " << (sizeof(float)*sort_partitions*k)/1024 << "KB (" << sort_partitions << " x " << k << ")" << std::endl;);
	DBG(std::cout << "    HeapI buf: " << (sizeof(cl_uint)*sort_partitions*k)/1024 << "KB (" << sort_partitions << " x " << k << ")" << std::endl;);
#if TIMER
	cl_ulong start = 0, end = 0;
	cl_double total = 0.0;
	time_t ltime;
	time(&ltime);
	double timeElapsed = 0.0;
	Timer *t;
	t = new Timer();
#endif
	/* Do work */
	int offset = 0;
	int workleft = q;
	int worksize; 
	do {
		worksize = min(workleft, knnOclWgSize);
		if (knn_wgobjects == 1) err = parallel_distances.setArg(7, offset); check_err(err, "Set kernel parallel_distances_worker arg offset"); // Used if q > wg
		err = knnOclQueue.enqueueNDRangeKernel(parallel_distances, cl::NullRange, cl::NDRange(n*worksize+knn_padding), cl::NDRange(worksize*knn_wgobjects), 0, &ocltimer_knn); check_err(err, "Enqueue parallel_distances kernel");
		offset += worksize;
		workleft -= worksize;
#if CL_PROFILING
		err = knnOclQueue.finish(); check_err(err, "Clear work queue");
		ocltimer_knn.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
		ocltimer_knn.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
		total += (cl_double)(end - start)*(cl_double)(1e-09);
#endif
		DBG(std::cout << "opencl_knn: worksize = " << worksize << " workleft = " << workleft << std::endl;);
	} while (workleft > 0);

#if !ZEROCOPY
	err = knnOclQueue.enqueueReadBuffer(distancesBuffer, CL_FALSE, 0, sizeof(cl_float)*n*q, distancesBufferHost, 0, &ocltimer_xfer); check_err(err, "Copy distancesBuffer from device");
	cl::Buffer sortedDistancesBufferPinned = cl::Buffer(sortOclContext, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, (int)sizeof(cl_float)*n*q, nullptr, &err); check_err(err, "Init sortedDistanceBufferPinned");
	float *sortedDistancesBufferHost = (float *)sortOclQueue.enqueueMapBuffer(sortedDistancesBufferPinned, CL_FALSE, CL_MAP_READ, 0, (int)sizeof(cl_float)*n*q, 0, 0, &err); check_err(err, "Map distancesBufferPinned");
	memcpy(sortedDistancesBufferHost, distancesBufferHost, sizeof(cl_float)*n*q);
	cl::Buffer sortedDistancesBuffer = cl::Buffer(sortOclContext, CL_MEM_READ_ONLY, (int)sizeof(cl_float)*n*q, nullptr, &err); check_err(err, "Init sortedDistanceBuffer");
	err = sortOclQueue.enqueueWriteBuffer(sortedDistancesBuffer, CL_TRUE, 0, (int)sizeof(cl_float)*n*q, sortedDistancesBufferHost, 0, 0); check_err(err, "Enqueue copy dataBuffer");
#else
	float *distancesBufferHost = (float *)knnOclQueue.enqueueMapBuffer(distancesBufferPinned, CL_TRUE, CL_MAP_READ, 0, (int)sizeof(cl_float)*n*q, 0, &ocltimer_xfer, &err); check_err(err, "Map distancesBufferPinned");
#endif

	/* Setup for sort_index */
#if ZEROCOPY
	cl::Buffer sortedDistancesBufferPinned = cl::Buffer(sortOclContext, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, (int)sizeof(cl_float)*n*q, *distances, &err); check_err(err, "Init sortedDistancesBufferPinned");
	cl::Buffer indexBufferPinned = cl::Buffer(sortOclContext, CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR, (int)sizeof(cl_int)*k*q, *index, &err); check_err(err, "Init indexBufferPinned");
	err = parallel_sort_index.setArg(0, sortedDistancesBufferPinned); check_err(err, "Set kernel parallel_minmaxheapsort_index arg distancesBufferPinned");
	err = parallel_sort_index.setArg(1, indexBufferPinned); check_err(err, "Set kernel parallel_minmaxheapsort_index arg indexBufferPinned");
#else
	/* Pinned host memory */
	cl::Buffer indexBufferPinned = cl::Buffer(sortOclContext, CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR, (int)sizeof(cl_int)*k*q, nullptr, &err); check_err(err, "Init indexBufferPinned");
	float *indexBufferHost = (float *)sortOclQueue.enqueueMapBuffer(indexBufferPinned, CL_FALSE, CL_MAP_WRITE, 0, (int)sizeof(cl_int)*k*q, 0, 0, &err); check_err(err, "Map indexBufferPinned");
	cl::Buffer indexBuffer = cl::Buffer(sortOclContext, CL_MEM_WRITE_ONLY, (int)sizeof(cl_int)*k*q, NULL, &err); check_err(err, "Init indexBuffer");

	err = parallel_sort_index.setArg(0, sortedDistancesBuffer); check_err(err, "Set kernel parallel_minmaxheapsort_index arg distancesBuffer");
	err = parallel_sort_index.setArg(1, indexBuffer); check_err(err, "Set kernel parallel_minmaxheapsort_index arg indexBuffer");
#endif
	err = parallel_sort_index.setArg(2, n); check_err(err, "Set kernel parallel_minmaxheapsort_index arg n");
	err = parallel_sort_index.setArg(3, q); check_err(err, "Set kernel parallel_minmaxheapsort_index arg q");
	err = parallel_sort_index.setArg(4, k); check_err(err, "Set kernel parallel_minmaxheapsort_index arg k");
#if !GLOBAL_SORT
	err = parallel_sort_index.setArg(5, cl::__local(k*sizeof(cl_float)*sort_partitions)); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapData (local)");
	err = parallel_sort_index.setArg(6, cl::__local(k*sizeof(cl_uint)*sort_partitions)); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapIdx (local)");
#else
	cl::Buffer heapDataBufferG = cl::Buffer(sortOclContext, CL_MEM_READ_WRITE, (int)sizeof(cl_float)*k*q, NULL, &err); check_err(err, "Init heapDataBuffer");
	cl::Buffer heapIndexBufferG = cl::Buffer(sortOclContext, CL_MEM_READ_WRITE, (int)sizeof(cl_int)*k*q, NULL, &err); check_err(err, "Init heapIndexBuffer");
	err = parallel_sort_index.setArg(5, heapDataBufferG); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapData (local)");
	err = parallel_sort_index.setArg(6, heapIndexBufferG); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapIdx (local)");
#endif

#if !GLOBAL_SORT
	err = sortOclQueue.enqueueNDRangeKernel(parallel_sort_index, cl::NullRange, cl::NDRange(q*sort_partitions), cl::NDRange(sort_partitions), 0, &ocltimer_sort); check_err(err, "Enqueue parallel_sort_index kernel");
	DBG(std::cout << "    Sort kern: " << q*sort_partitions << " x " << sort_partitions << std::endl;);
#else
	err = sortOclQueue.enqueueNDRangeKernel(parallel_sort_index, cl::NullRange, cl::NDRange(q), cl::NDRange(1), 0, &ocltimer_sort); check_err(err, "Enqueue parallel_sort_index kernel");
#endif

#if !ZEROCOPY
	err = sortOclQueue.enqueueReadBuffer(indexBuffer, CL_FALSE, 0, sizeof(cl_int)*k*q, indexBufferHost, 0, 0); check_err(err, "Copy indexBuffer from device");
#else
	int *indexBufferHost = (int *)sortOclQueue.enqueueMapBuffer(indexBufferPinned, CL_FALSE, CL_MAP_READ, 0, (int)sizeof(cl_int)*k*q, 0, 0, &err); check_err(err, "Map indexBufferPinned");
#if DUMPOUTPUT
	//float *distancesBufferHost = (float *)knnOclQueue.enqueueMapBuffer(distancesBufferPinned, CL_FALSE, CL_MAP_READ, 0, (int)sizeof(cl_float)*n*q, 0, 0, &err); check_err(err, "Map distancesBufferPinned");
#endif
#endif
	err = knnOclQueue.finish(); check_err(err, "Clear work knnOclQueue");
	err = sortOclQueue.finish(); check_err(err, "Clear work sortOclQueue");

#if !ZEROCOPY
	/* Copy back into original arrays */
	memcpy(*index, indexBufferHost, sizeof(cl_uint)*k*q);
#if DUMPOUTPUT
	memcpy(*distances, distancesBufferHost, sizeof(cl_float)*n*q);
#endif
#endif

#if TIMER
	timeElapsed = t->elapsed();
#endif

#if CL_PROFILING
	std::cout << "OpenCL Device distance execution time: " << total << "s" << std::endl;
	std::cout << "                     distance iterated " << (q/knnOclWgSize) + 1 << " times" << std::endl;
	start = 0, end = 0;
	ocltimer_xfer.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
	ocltimer_xfer.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
	std::cout << "OpenCL Inter-device transfer time:     " << (cl_double)(end - start)*(cl_double)(1e-09) << "s" << std::endl;
	start = 0, end = 0;
	ocltimer_sort.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
	ocltimer_sort.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
	std::cout << "OpenCL Device sort execution time:     " << (cl_double)(end - start)*(cl_double)(1e-09) << "s" << std::endl;
#endif

#if TIMER
	std::cout << "OpenCL Host KNN time: " << timeElapsed << "s" << std::endl;
	delete t;
#endif
}

/* Minmax Heapsort only */
void opencl_minmaxheapsort_index(float *input[], int *index[], int n, int q, int k, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	/* More init code borrowed from ranluxcltest.cpp */
	/* OpenCL variables */
	cl::Context context;
	std::vector <cl::Device> devices;
	cl::CommandQueue queue;
	cl::Program program;

	OpenCL_initializations(context, devices, queue, DEBUG, CL_PROFILING, useGPU, openclDevice, openclPlatform);

	/* Building OpenCL program */
	std::string BuildOptions;
	std::string BuildLog;
	std::string FileName = CL_SORT;
	BuildOptions += "-cl-fast-relaxed-math -I . "; // Search for include files in current directory
	compile_OpenCL_code(BuildOptions, FileName, BuildLog, context, program, devices, openclDevice);

	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	cl_int err;

	/* Setting kernels */
#if GLOBAL_SORT
	cl::Kernel parallel_sort_index = cl::Kernel(program, "parallel_global_minmaxheapsort_index", &err); check_err(err, "Kernel parallel_global_minmaxheapsort_index");
#else
	cl::Kernel parallel_sort_index = cl::Kernel(program, "parallel_minmaxheapsort_index", &err); check_err(err, "Kernel parallel_minmaxheapsort_index");
#endif

	/* Setup for sort_index */
	cl::Buffer inputBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*n*q, *input, &err); check_err(err, "Init inputBuffer");
	cl::Buffer indexBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, (int)sizeof(cl_int)*n*q, NULL, &err); check_err(err, "Init indexBuffer");

	err = parallel_sort_index.setArg(0, inputBuffer); check_err(err, "Set kernel parallel_minmaxheapsort_index arg inputBuffer");
	err = parallel_sort_index.setArg(1, indexBuffer); check_err(err, "Set kernel parallel_minmaxheapsort_index arg indexBuffer");
	err = parallel_sort_index.setArg(2, n); check_err(err, "Set kernel parallel_minmaxheapsort_index arg n");
	err = parallel_sort_index.setArg(3, q); check_err(err, "Set kernel parallel_minmaxheapsort_index arg q");
	err = parallel_sort_index.setArg(4, k); check_err(err, "Set kernel parallel_minmaxheapsort_index arg k");
#if !GLOBAL_SORT
	err = parallel_sort_index.setArg(5, cl::__local(k*sizeof(cl_float))); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapData (local)");
	err = parallel_sort_index.setArg(6, cl::__local(k*sizeof(cl_uint))); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapIdx (local)");
#else
	cl::Buffer heapDataBufferG = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_float)*k*q, NULL, &err); check_err(err, "Init heapDataBuffer");
	cl::Buffer heapIndexBufferG = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_int)*k*q, NULL, &err); check_err(err, "Init heapIndexBuffer");
	err = parallel_sort_index.setArg(5, heapDataBufferG); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapData (local)");
	err = parallel_sort_index.setArg(6, heapIndexBufferG); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapIdx (local)");
#endif

#if TIMER
	time_t ltime;
	time(&ltime);
	double timeElapsed = 0.0;
	Timer *t;
	t = new Timer();
#endif

	err = queue.enqueueNDRangeKernel(parallel_sort_index, cl::NullRange, cl::NDRange(q), cl::NDRange(1), 0, 0); check_err(err, "Enqueue parallel_sort_index kernel");

	//err = queue.finish(); check_err(err, "Clear work queue");

	/* Download results from device */
	err = queue.enqueueReadBuffer(indexBuffer, CL_TRUE, 0, sizeof(cl_int)*k*q, *index, 0, 0); check_err(err, "Copy indexBuffer from device"); // only first k elements are valid
#if TIMER
	timeElapsed = t->elapsed();
	std::cout << "OpenCL parallel_minmaxheapsort_index time: " << timeElapsed << "s" << std::endl;
	delete t;
#endif
	/* Done with buffers */
	inputBuffer = cl::Buffer();
	indexBuffer = cl::Buffer();
}

/* Minmax Heapsort w/ multiple heap reduction only */
void opencl_reduce_minmaxheapsort_index(float *input[], int *index[], int n, int q, int k, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	/* More init code borrowed from ranluxcltest.cpp */
	/* OpenCL variables */
	cl::Context context;
	std::vector <cl::Device> devices;
	cl::CommandQueue queue;
	cl::Program program;

	OpenCL_initializations(context, devices, queue, DEBUG, CL_PROFILING, useGPU, openclDevice, openclPlatform);

	/* Building OpenCL program */
	std::string BuildOptions;
	std::string BuildLog;
	std::string FileName = CL_SORT;
	BuildOptions += "-cl-fast-relaxed-math -I . "; // Search for include files in current directory
	compile_OpenCL_code(BuildOptions, FileName, BuildLog, context, program, devices, openclDevice);

	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	cl_int err;

	/* Determine number of sort partitions */
	unsigned int partitions = 1;
	if (openclPlatform == NV_OPENCLPLATFORM) { // Nvidia, warp size 32, local mem 48KB
		if (k <= 6 && n >= 1024*k) partitions = 1024;
		else if (k <= 12 && n >= 512*k) partitions = 512;
		else if (k <= 24 && n >= 256*k) partitions = 256;
		else if (k <= 48 && n >= 128*k) partitions = 128;
		else if (k <= 96 && n >= 64*k) partitions = 64;
		else if (k <= 192 && n >= 32*k) partitions = 32;
#if USE_SMALL_PARTITIONS
		else if (k <= 384 && n >= 16*k) partitions = 16;
		//else if (k <= 768 && n >= 8*k) partitions = 8;
		//else if (k <= 1536 && n >= 4*k) partitions = 4;
		//else if (k <= 3072 && n >= 2*k) partitions = 2;
#endif
	} else if (openclPlatform == AMD_OPENCLPLATFORM) { // AMD, warp (wavefront) size 64, local mem 32KB
		if (!useGPU && k <= 4 && n >= 1024*k) partitions = 1024;
		else if (!useGPU && k <= 8 && n >= 512*k) partitions = 512;
		else if (k <= 16 && n >= 256*k) partitions = 256; // AMD GPUs max workgroup size 256
		else if (k <= 32 && n >= 128*k) partitions = 128;
		else if (k <= 64 && n >= 64*k) partitions = 64;
#if USE_SMALL_PARTITIONS
		else if (k <= 128 && n >= 32*k) partitions = 32;
		else if (k <= 256 && n >= 16*k) partitions = 16;
		//else if (k <= 512 && n >= 8*k) partitions = 8;
		//else if (k <= 1024 && n >= 4*k) partitions = 4;
		//else if (k <= 2048 && n >= 2*k) partitions = 2;
#endif
	}

	/* Setting kernels */
	cl::Kernel parallel_sort_index;

#if GLOBAL_SORT
	partitions = 1;
	parallel_sort_index = cl::Kernel(program, "parallel_global_minmaxheapsort_index", &err); check_err(err, "Kernel parallel_global_minmaxheapsort_index");
#else
	if (sort_partitions == 1) {
		parallel_sort_index = cl::Kernel(program, "parallel_minmaxheapsort_index", &err); check_err(err, "Kernel parallel_minmaxheapsort_index");
	} else {
		parallel_sort_index = cl::Kernel(program, "parallel_reduce_minmaxheapsort_index", &err); check_err(err, "Kernel parallel_reduce_minmaxheapsort_index");
	}
#endif

	DBG(std::cout << "opencl_reduce_minmaxheapsort_index: partitions = " << partitions << std::endl;);

	/* Setup for sort_index */
	cl::Buffer inputBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*n*q, *input, &err); check_err(err, "Init inputBuffer");
	cl::Buffer indexBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, (int)sizeof(cl_int)*n*q, NULL, &err); check_err(err, "Init indexBuffer");

	err = parallel_sort_index.setArg(0, inputBuffer); check_err(err, "Set kernel parallel_minmaxheapsort_index arg inputBuffer");
	err = parallel_sort_index.setArg(1, indexBuffer); check_err(err, "Set kernel parallel_minmaxheapsort_index arg indexBuffer");
	err = parallel_sort_index.setArg(2, n); check_err(err, "Set kernel parallel_minmaxheapsort_index arg n");
	err = parallel_sort_index.setArg(3, q); check_err(err, "Set kernel parallel_minmaxheapsort_index arg q");
	err = parallel_sort_index.setArg(4, k); check_err(err, "Set kernel parallel_minmaxheapsort_index arg k");
#if !GLOBAL_SORT
	err = parallel_sort_index.setArg(5, cl::__local(k*sizeof(cl_float))); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapData (local)");
	err = parallel_sort_index.setArg(6, cl::__local(k*sizeof(cl_uint))); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapIdx (local)");
#else
	cl::Buffer heapDataBufferG = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_float)*k*q, NULL, &err); check_err(err, "Init heapDataBuffer");
	cl::Buffer heapIndexBufferG = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_int)*k*q, NULL, &err); check_err(err, "Init heapIndexBuffer");
	err = parallel_sort_index.setArg(5, heapDataBufferG); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapData (local)");
	err = parallel_sort_index.setArg(6, heapIndexBufferG); check_err(err, "Set kernel parallel_minmaxheapsort_index argument heapIdx (local)");
#endif

#if TIMER
	time_t ltime;
	time(&ltime);
	double timeElapsed = 0.0;
	Timer *t;
	t = new Timer();
#endif

#if !GLOBAL_SORT
	err = queue.enqueueNDRangeKernel(parallel_sort_index, cl::NullRange, cl::NDRange(q*partitions), cl::NDRange(partitions), 0, 0); check_err(err, "Enqueue parallel_sort_index kernel");
#else
	err = queue.enqueueNDRangeKernel(parallel_sort_index, cl::NullRange, cl::NDRange(q), cl::NDRange(1), 0, 0); check_err(err, "Enqueue parallel_sort_index kernel");
#endif

	//err = queue.finish(); check_err(err, "Clear work queue");

	/* Download results from device */
	err = queue.enqueueReadBuffer(indexBuffer, CL_TRUE, 0, sizeof(cl_int)*k*q, *index, 0, 0); check_err(err, "Copy indexBuffer from device");
#if TIMER
	timeElapsed = t->elapsed();
	std::cout << "OpenCL parallel_minmaxheapsort_index time: " << timeElapsed << "s" << std::endl;
	delete t;
#endif
	/* Done with buffers */
	inputBuffer = cl::Buffer();
	indexBuffer = cl::Buffer();
}


/* Max item only */
void opencl_minmaxheapsort_max(float *input[], float *maxval[], int n, int q, int k, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	/* More init code borrowed from ranluxcltest.cpp */
	/* OpenCL variables */
	cl::Context context;
	std::vector <cl::Device> devices;
	cl::CommandQueue queue;
	cl::Program program;
	cl::Event ocltimer;

	OpenCL_initializations(context, devices, queue, DEBUG, CL_PROFILING, useGPU, openclDevice, openclPlatform);

	/* Building OpenCL program */
	std::string BuildOptions;
	std::string BuildLog;
	std::string FileName = CL_SORT;
	BuildOptions += "-cl-fast-relaxed-math -I . "; // Search for include files in current directory
	compile_OpenCL_code(BuildOptions, FileName, BuildLog, context, program, devices, openclDevice);

	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	cl_int err;

	/* Setting kernels */
#if GLOBAL_SORT
	cl::Kernel parallel_sort_index = cl::Kernel(program, "parallel_global_minmaxheapsort_max", &err); check_err(err, "Kernel parallel_global_minmaxheapsort_max");
#else
	cl::Kernel parallel_sort_index = cl::Kernel(program, "parallel_minmaxheapsort_max", &err); check_err(err, "Kernel parallel_minmaxheapsort_index");
#endif

	/* Setup for sort_index */
	cl::Buffer inputBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*n*q, *input, &err); check_err(err, "Init inputBuffer");
	cl::Buffer maxvalBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, (int)sizeof(cl_float)*q, NULL, &err); check_err(err, "Init maxvalBuffer");

	err = parallel_sort_index.setArg(0, inputBuffer); check_err(err, "Set kernel parallel_minmaxheapsort_max arg inputBuffer");
	err = parallel_sort_index.setArg(1, maxvalBuffer); check_err(err, "Set kernel parallel_minmaxheapsort_max arg indexBuffer");
	err = parallel_sort_index.setArg(2, n); check_err(err, "Set kernel parallel_minmaxheapsort_max arg n");
	err = parallel_sort_index.setArg(3, q); check_err(err, "Set kernel parallel_minmaxheapsort_max arg q");
	err = parallel_sort_index.setArg(4, k); check_err(err, "Set kernel parallel_minmaxheapsort_max arg k");
#if !GLOBAL_SORT
	err = parallel_sort_index.setArg(5, cl::__local(k*sizeof(cl_float))); check_err(err, "Set kernel parallel_minmaxheapsort_max argument heapData (local)");
	err = parallel_sort_index.setArg(6, cl::__local(k*sizeof(cl_uint))); check_err(err, "Set kernel parallel_minmaxheapsort_max argument heapIdx (local)");
#else
	cl::Buffer heapDataBufferG = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_float)*k*q, NULL, &err); check_err(err, "Init heapDataBuffer");
	cl::Buffer heapIndexBufferG = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_int)*k*q, NULL, &err); check_err(err, "Init heapIndexBuffer");
	err = parallel_sort_index.setArg(5, heapDataBufferG); check_err(err, "Set kernel parallel_minmaxheapsort_max argument heapData (local)");
	err = parallel_sort_index.setArg(6, heapIndexBufferG); check_err(err, "Set kernel parallel_minmaxheapsort_max argument heapIdx (local)");
#endif

#if TIMER
	time_t ltime;
	time(&ltime);
	double timeElapsed = 0.0;
	Timer *t;
	t = new Timer();
#endif

	err = queue.enqueueNDRangeKernel(parallel_sort_index, cl::NullRange, cl::NDRange(q), cl::NDRange(1), 0, &ocltimer); check_err(err, "Enqueue parallel_minmaxheapsort_max kernel");

	//err = queue.finish(); check_err(err, "Clear work queue");

	/* Download results from device */
	err = queue.enqueueReadBuffer(maxvalBuffer, CL_TRUE, 0, sizeof(cl_float)*q, *maxval, 0, 0); check_err(err, "Copy maxvalBuffer from device"); // only first k elements are valid
#if TIMER
	timeElapsed = t->elapsed();
	std::cout << "OpenCL Host parallel_minmaxheapsort_max time: " << timeElapsed << "s" << std::endl;
	delete t;

#if CL_PROFILING
	cl_ulong start = 0, end = 0;
	ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
	ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
	std::cout << "OpenCL Device parallel_minmaxheapsort_max time: " << (cl_double)(end - start)*(cl_double)(1e-09) << "s" << std::endl;
#endif
#endif
	/* Done with buffers */
	inputBuffer = cl::Buffer();
	maxvalBuffer = cl::Buffer();
}

void opencl_reduce_minmaxheapsort_max(float *input[], float *maxval[], int n, int q, int k, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	/* More init code borrowed from ranluxcltest.cpp */
	/* OpenCL variables */
	cl::Context context;
	std::vector <cl::Device> devices;
	cl::CommandQueue queue;
	cl::Program program;
	cl::Event ocltimer;

	OpenCL_initializations(context, devices, queue, DEBUG, CL_PROFILING, useGPU, openclDevice, openclPlatform);

	/* Building OpenCL program */
	std::string BuildOptions;
	std::string BuildLog;
	std::string FileName = CL_SORT;
	BuildOptions += "-cl-fast-relaxed-math -I . "; // Search for include files in current directory
	compile_OpenCL_code(BuildOptions, FileName, BuildLog, context, program, devices, openclDevice);

	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	cl_int err;

	/* Determine number of sort partitions */
	unsigned int partitions = 1;
	if (openclPlatform == NV_OPENCLPLATFORM) { // Nvidia, warp size 32, local mem 48KB
		if (k <= 6 && n >= 1024*k) partitions = 1024;
		else if (k <= 12 && n >= 512*k) partitions = 512;
		else if (k <= 24 && n >= 256*k) partitions = 256;
		else if (k <= 48 && n >= 128*k) partitions = 128;
		else if (k <= 96 && n >= 64*k) partitions = 64;
		else if (k <= 192 && n >= 32*k) partitions = 32;
#if USE_SMALL_PARTITIONS
		else if (k <= 384 && n >= 16*k) partitions = 16;
		//else if (k <= 768 && n >= 8*k) partitions = 8;
		//else if (k <= 1536 && n >= 4*k) partitions = 4;
		//else if (k <= 3072 && n >= 2*k) partitions = 2;
#endif
	} else if (openclPlatform == AMD_OPENCLPLATFORM) { // AMD, warp (wavefront) size 64, local mem 32KB
		if (!useGPU && k <= 4 && n >= 1024*k) partitions = 1024;
		else if (!useGPU && k <= 8 && n >= 512*k) partitions = 512;
		else if (k <= 16 && n >= 256*k) partitions = 256; // AMD GPUs max workgroup size 256
		else if (k <= 32 && n >= 128*k) partitions = 128;
		else if (k <= 64 && n >= 64*k) partitions = 64;
#if USE_SMALL_PARTITIONS
		else if (k <= 128 && n >= 32*k) partitions = 32;
		else if (k <= 256 && n >= 16*k) partitions = 16;
		//else if (k <= 512 && n >= 8*k) partitions = 8;
		//else if (k <= 1024 && n >= 4*k) partitions = 4;
		//else if (k <= 2048 && n >= 2*k) partitions = 2;
#endif
	}

	/* Setting kernels */
	cl::Kernel parallel_sort_index;

#if GLOBAL_SORT
	partitions = 1;
	parallel_sort_index = cl::Kernel(program, "parallel_global_minmaxheapsort_max", &err); check_err(err, "Kernel parallel_global_minmaxheapsort_max");
#else
	if (sort_partitions == 1) {
		parallel_sort_index = cl::Kernel(program, "parallel_minmaxheapsort_max", &err); check_err(err, "Kernel parallel_minmaxheapsort_max");
	} else {
		parallel_sort_index = cl::Kernel(program, "parallel_reduce_minmaxheapsort_max", &err); check_err(err, "Kernel parallel_reduce_minmaxheapsort_max");
	}
#endif

	DBG(std::cout << "parallel_reduce_minmaxheapsort_max: partitions = " << partitions << std::endl;);

	/* Setup for sort_index */
	cl::Buffer inputBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*n*q, *input, &err); check_err(err, "Init inputBuffer");
	cl::Buffer maxvalBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, (int)sizeof(cl_float)*q, NULL, &err); check_err(err, "Init maxvalBuffer");

	err = parallel_sort_index.setArg(0, inputBuffer); check_err(err, "Set kernel parallel_minmaxheapsort_max arg inputBuffer");
	err = parallel_sort_index.setArg(1, maxvalBuffer); check_err(err, "Set kernel parallel_minmaxheapsort_max arg maxvalBuffer");
	err = parallel_sort_index.setArg(2, n); check_err(err, "Set kernel parallel_minmaxheapsort_max arg n");
	err = parallel_sort_index.setArg(3, q); check_err(err, "Set kernel parallel_minmaxheapsort_max arg q");
	err = parallel_sort_index.setArg(4, k); check_err(err, "Set kernel parallel_minmaxheapsort_max arg k");
#if !GLOBAL_SORT
	err = parallel_sort_index.setArg(5, cl::__local(k*sizeof(cl_float))); check_err(err, "Set kernel parallel_minmaxheapsort_max argument heapData (local)");
	err = parallel_sort_index.setArg(6, cl::__local(k*sizeof(cl_uint))); check_err(err, "Set kernel parallel_minmaxheapsort_max argument heapIdx (local)");
#else
	cl::Buffer heapDataBufferG = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_float)*k*q, NULL, &err); check_err(err, "Init heapDataBuffer");
	cl::Buffer heapIndexBufferG = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_int)*k*q, NULL, &err); check_err(err, "Init heapIndexBuffer");
	err = parallel_sort_index.setArg(5, heapDataBufferG); check_err(err, "Set kernel parallel_minmaxheapsort_max argument heapData (local)");
	err = parallel_sort_index.setArg(6, heapIndexBufferG); check_err(err, "Set kernel parallel_minmaxheapsort_max argument heapIdx (local)");
#endif

#if TIMER
	time_t ltime;
	time(&ltime);
	double timeElapsed = 0.0;
	Timer *t;
	t = new Timer();
#endif

#if !GLOBAL_SORT
	err = queue.enqueueNDRangeKernel(parallel_sort_index, cl::NullRange, cl::NDRange(q*partitions), cl::NDRange(partitions), 0, &ocltimer); check_err(err, "Enqueue parallel_minmaxheapsort_max kernel");
#else
	err = queue.enqueueNDRangeKernel(parallel_sort_index, cl::NullRange, cl::NDRange(q), cl::NDRange(1), 0, &ocltimer); check_err(err, "Enqueue parallel_minmaxheapsort_max kernel");
#endif

	//err = queue.finish(); check_err(err, "Clear work queue");

	/* Download results from device */
	err = queue.enqueueReadBuffer(maxvalBuffer, CL_TRUE, 0, sizeof(cl_float)*q, *maxval, 0, 0); check_err(err, "Copy indexBuffer from device"); // only first k elements are valid
#if TIMER
	timeElapsed = t->elapsed();
	std::cout << "OpenCL Host parallel_minmaxheapsort_max time: " << timeElapsed << "s" << std::endl;
	delete t;

#if CL_PROFILING
	cl_ulong start = 0, end = 0;
	ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
	ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
	std::cout << "OpenCL Device parallel_minmaxheapsort_max time: " << (cl_double)(end - start)*(cl_double)(1e-09) << "s" << std::endl;
#endif
#endif
	/* Done with buffers */
	inputBuffer = cl::Buffer();
	maxvalBuffer = cl::Buffer();
}


/* Threshold */
void opencl_minmaxheapsort_threshold(float *input[], int *index[], float *thresholds[], int n, int q, int k, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	/* More init code borrowed from ranluxcltest.cpp */
	/* OpenCL variables */
	cl::Context context;
	std::vector <cl::Device> devices;
	cl::CommandQueue queue;
	cl::Program program;
	cl::Event ocltimer;

	OpenCL_initializations(context, devices, queue, DEBUG, CL_PROFILING, useGPU, openclDevice, openclPlatform);

	/* Building OpenCL program */
	std::string BuildOptions;
	std::string BuildLog;
	std::string FileName = CL_SORT;
	BuildOptions += "-cl-fast-relaxed-math -I . "; // Search for include files in current directory
	compile_OpenCL_code(BuildOptions, FileName, BuildLog, context, program, devices, openclDevice);

	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	cl_int err;

	/* Setting kernels */
	cl::Kernel parallel_sort_index = cl::Kernel(program, "parallel_global_minmaxheapsort_index", &err); check_err(err, "Kernel parallel_global_minmaxheapsort_index");

	/* Setup */
	cl::Buffer inputBuffer;
	cl::Buffer thresholdsBuffer;
	cl::Buffer indexBuffer;
	cl::Buffer heapDataBufferG;
	cl::Buffer heapIndexBufferG;

	const int cbatchsize = 4;

#if TIMER
	cl_ulong start = 0, end = 0; 
	cl_double total = 0.0;

	time_t ltime;
	time(&ltime);
	double timeElapsed = 0.0;
	Timer *t;
	t = new Timer();
#endif

	int compressedsize, notpruned, cidx, cdim;
	int64_t totalcompressedsize = 0;
	float *cInput;
	int *cIndex, *cMapping;
	for (int qwkbatch = 0; qwkbatch < q/cbatchsize; qwkbatch++) {
		/* Create compressed buffers */
		compressedsize = 0;
		for (int d = qwkbatch*cbatchsize; d < (qwkbatch*cbatchsize)+cbatchsize; d++) {
			notpruned = 0;
			for (int i = 0; i < n; i++) {
				if ((*input)[d*n+i] <= (*thresholds)[d]) notpruned++;
			}
			compressedsize = max(compressedsize, notpruned);
		}
		totalcompressedsize += compressedsize;

		cInput = (float *)realloc(NULL, sizeof(float)*compressedsize*cbatchsize); check_alloc(cInput);
		cIndex = (int *)realloc(NULL, sizeof(int)*k*cbatchsize); check_alloc(cIndex);
		cMapping = (int *)realloc(NULL, sizeof(int)*compressedsize*cbatchsize); check_alloc(cMapping);

		for (int d = qwkbatch*cbatchsize; d < (qwkbatch*cbatchsize)+cbatchsize; d++) {
			cidx = 0;
			for (int i = 0; i < n; i++) {
				if ((*input)[d*n+i] <= (*thresholds)[d]) {
					cInput[(d-(qwkbatch*cbatchsize))*compressedsize+cidx] = (*input)[d*n+i];
					cMapping[(d-(qwkbatch*cbatchsize))*compressedsize+cidx] = i;
					cidx++;
				}
			}
			while (cidx < compressedsize) {
				cInput[(d-(qwkbatch*cbatchsize))*compressedsize+cidx] = FLT_MAX;
				cidx++;
			}
		}

		inputBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*compressedsize*cbatchsize, cInput, &err); check_err(err, "Init inputBuffer");
		indexBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, (int)sizeof(cl_int)*k*cbatchsize, NULL, &err); check_err(err, "Init indexBuffer");

		heapDataBufferG = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_float)*k*cbatchsize, NULL, &err); check_err(err, "Init heapDataBuffer");
		heapIndexBufferG = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_int)*k*cbatchsize, NULL, &err); check_err(err, "Init heapIndexBuffer");

		err = parallel_sort_index.setArg(0, inputBuffer); check_err(err, "Set kernel parallel_global_minmaxheapsort_index arg inputBuffer");
		err = parallel_sort_index.setArg(1, indexBuffer); check_err(err, "Set kernel parallel_global_minmaxheapsort_index arg indexBuffer");
		err = parallel_sort_index.setArg(2, compressedsize); check_err(err, "Set kernel parallel_global_minmaxheapsort_index arg n");
		err = parallel_sort_index.setArg(3, cbatchsize); check_err(err, "Set kernel parallel_global_minmaxheapsort_index arg q");
		err = parallel_sort_index.setArg(4, k); check_err(err, "Set kernel parallel_global_minmaxheapsort_index arg k");
		err = parallel_sort_index.setArg(5, heapDataBufferG); check_err(err, "Set kernel parallel_global_minmaxheapsort_index argument heapData");
		err = parallel_sort_index.setArg(6, heapIndexBufferG); check_err(err, "Set kernel parallel_global_minmaxheapsort_index argument heapIdx");

		err = queue.enqueueNDRangeKernel(parallel_sort_index, cl::NullRange, cl::NDRange(cbatchsize), cl::NDRange(1), 0, &ocltimer); check_err(err, "Enqueue parallel_global_minmaxheapsort_index kernel");

		//err = queue.finish(); check_err(err, "Clear work queue");

		/* Download results from device */
		err = queue.enqueueReadBuffer(indexBuffer, CL_TRUE, 0, sizeof(cl_float)*k*cbatchsize, cIndex, 0, 0); check_err(err, "Copy indexBuffer from device");

		for (int i = 0; i < k; i++) {
			for (int d = qwkbatch*cbatchsize; d < (qwkbatch*cbatchsize)+cbatchsize; d++) {
				(*index)[i*q+d] = cMapping[(d-(qwkbatch*cbatchsize))*compressedsize+cIndex[i*cbatchsize+(d-(qwkbatch*cbatchsize))]];
			}
		}

#if CL_PROFILING
		err = queue.finish(); check_err(err, "Clear work queue");
		ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
		ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
		total += (cl_double)(end - start)*(cl_double)(1e-09);
#endif
	}
#if TIMER
	timeElapsed = t->elapsed();
	std::cout << "OpenCL Host parallel_global_minmaxheapsort_threshold time: " << timeElapsed << "s" << std::endl;
	delete t;

#if CL_PROFILING
	std::cout << "OpenCL Device parallel_global_minmaxheapsort_threshold time: " << total << "s" << std::endl;
#endif
#endif

	std::cout << "Compression ratio: " << (double)(n*(q/cbatchsize))/totalcompressedsize << std::endl;

	/* Done with buffers */
	inputBuffer = cl::Buffer();
	thresholdsBuffer = cl::Buffer();
	indexBuffer = cl::Buffer();
	heapDataBufferG = cl::Buffer();
	heapIndexBufferG = cl::Buffer();
}



/* KNN distance only */
void opencl_distances(float *data[], float *distances[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	/* More init code borrowed from ranluxcltest.cpp */
	/* OpenCL variables */
	cl::Context context;
	std::vector <cl::Device> devices;
	cl::CommandQueue queue;
	cl::Program program;
	cl::Event ocltimer;

	OpenCL_initializations(context, devices, queue, DEBUG, CL_PROFILING, useGPU, openclDevice, openclPlatform);

	/* Building OpenCL program */
	std::string BuildOptions;
	std::string BuildLog;
	std::string FileName = CL_KNN;
	BuildOptions += "-cl-mad-enable -cl-no-signed-zeros -I . "; // Search for include files in current directory
	compile_OpenCL_code(BuildOptions, FileName, BuildLog, context, program, devices, openclDevice);

	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	cl_int err;

	/* Setting kernels */
	cl::Kernel parallel_distances = cl::Kernel(program, "parallel_distances_worker", &err); check_err(err, "Kernel parallel_distances_cached_worker");

	/* Setup for parallel_distances */
	cl::Buffer dataBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*n*r, *data, &err); check_err(err, "Init dataBuffer");
	cl::Buffer distancesBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, (int)sizeof(cl_int)*n*q, NULL, &err); check_err(err, "Init indexBuffer");
	cl::Buffer queriesBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*q*r, *queries, &err); check_err(err, "Init queriesBuffer");

	err = parallel_distances.setArg(0, dataBuffer); check_err(err, "Set kernel parallel_distances_worker arg inputBuffer");
	err = parallel_distances.setArg(1, distancesBuffer); check_err(err, "Set kernel parallel_distances_worker arg distancesBuffer");
	err = parallel_distances.setArg(2, n); check_err(err, "Set kernel parallel_distances_worker arg n");
	err = parallel_distances.setArg(3, r); check_err(err, "Set kernel parallel_distances_worker arg r");
	err = parallel_distances.setArg(4, queriesBuffer); check_err(err, "Set kernel parallel_distances_worker arg queriesBuffer");
	err = parallel_distances.setArg(5, q); check_err(err, "Set kernel parallel_distances_worker arg q");

#if TIMER
	time_t ltime;
	time(&ltime);
	double timeElapsed = 0.0;
	Timer *t;
	t = new Timer();
#endif
	/* Do work: distances */
	err = queue.enqueueNDRangeKernel(parallel_distances, cl::NullRange, cl::NDRange(n*q), cl::NDRange(q), 0, &ocltimer); check_err(err, "Enqueue parallel_distances kernel");

	//err = queue.finish(); check_err(err, "Clear work queue");

	/* Download results from device */
	err = queue.enqueueReadBuffer(distancesBuffer, CL_TRUE, 0, sizeof(cl_float)*n*q, *distances, 0, 0); check_err(err, "Copy distancesBuffer from device");
#if TIMER
	timeElapsed = t->elapsed();
	std::cout << "OpenCL Host parallel_distances time: " << timeElapsed << "s" << std::endl;
	delete t;
#if CL_PROFILING
	cl_ulong start = 0, end = 0;
	ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
	ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
	std::cout << "OpenCL Device parallel_distances time: " << (cl_double)(end - start)*(cl_double)(1e-09) << "s" << std::endl;
#endif
#endif
	/* Done with buffers */
	dataBuffer = cl::Buffer();
	distancesBuffer = cl::Buffer();
	queriesBuffer = cl::Buffer();
}

/* KNN distance only w/ object caching only (test) */
void opencl_cached_smallq_distances(float *data[], float *distances[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount, unsigned int workgroupObjects) {
	/* More init code borrowed from ranluxcltest.cpp */
	/* OpenCL variables */
	cl::Context context;
	std::vector <cl::Device> devices;
	cl::CommandQueue queue;
	cl::Program program;
	cl::Event ocltimer;

	OpenCL_initializations(context, devices, queue, DEBUG, CL_PROFILING, useGPU, openclDevice, openclPlatform);

	/* Building OpenCL program */
	std::string BuildOptions;
	std::string BuildLog;
	std::string FileName = CL_KNN;
	BuildOptions += "-cl-mad-enable -cl-no-signed-zeros -I . "; // Search for include files in current directory
	compile_OpenCL_code(BuildOptions, FileName, BuildLog, context, program, devices, openclDevice);

	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	cl_int err;

	unsigned int padding = (q*workgroupObjects) - ((n*q)%(q*workgroupObjects));

	/* Setting kernels */
	cl::Kernel parallel_distances = cl::Kernel(program, "parallel_distances_cached_multi_worker", &err); check_err(err, "Kernel parallel_distances_cached_worker");

	/* Setup for parallel_distances */
	cl::Buffer dataBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*n*r, *data, &err); check_err(err, "Init dataBuffer");
	cl::Buffer distancesBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, (int)sizeof(cl_int)*n*q, NULL, &err); check_err(err, "Init indexBuffer");
	cl::Buffer queriesBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*q*r, *queries, &err); check_err(err, "Init queriesBuffer");

	err = parallel_distances.setArg(0, dataBuffer); check_err(err, "Set kernel parallel_distances_worker arg inputBuffer");
	err = parallel_distances.setArg(1, distancesBuffer); check_err(err, "Set kernel parallel_distances_worker arg distancesBuffer");
	err = parallel_distances.setArg(2, n); check_err(err, "Set kernel parallel_distances_worker arg n");
	err = parallel_distances.setArg(3, r); check_err(err, "Set kernel parallel_distances_worker arg r");
	err = parallel_distances.setArg(4, queriesBuffer); check_err(err, "Set kernel parallel_distances_worker arg queriesBuffer");
	err = parallel_distances.setArg(5, q); check_err(err, "Set kernel parallel_distances_worker arg q");
	err = parallel_distances.setArg(6, cl::__local(workgroupObjects*r*sizeof(cl_float))); check_err(err, "Set kernel parallel_distances_worker argument ldata (local)");

#if TIMER
	time_t ltime;
	time(&ltime);
	double timeElapsed = 0.0;
	Timer *t;
	t = new Timer();
#endif
	/* Do work: distances */
	err = queue.enqueueNDRangeKernel(parallel_distances, cl::NullRange, cl::NDRange(n*q+padding), cl::NDRange(q*workgroupObjects), 0, &ocltimer); check_err(err, "Enqueue parallel_distances kernel");

	//err = queue.finish(); check_err(err, "Clear work queue");

	/* Download results from device */
	err = queue.enqueueReadBuffer(distancesBuffer, CL_TRUE, 0, sizeof(cl_float)*n*q, *distances, 0, 0); check_err(err, "Copy distancesBuffer from device");
#if TIMER
	timeElapsed = t->elapsed();
	std::cout << "OpenCL parallel_distances time: " << timeElapsed << "s" << std::endl;
	delete t;
#if CL_PROFILING
	cl_ulong start = 0, end = 0;
	ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
	ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
	std::cout << "OpenCL Device parallel_distances time: " << (cl_double)(end - start)*(cl_double)(1e-09) << "s" << std::endl;
#endif
#endif
	/* Done with buffers */
	dataBuffer = cl::Buffer();
	distancesBuffer = cl::Buffer();
	queriesBuffer = cl::Buffer();
}

/* KNN distance only w/ object caching and optional multipass only */
void opencl_cached_multipass_distances(float *data[], float *distances[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	/* More init code borrowed from ranluxcltest.cpp */
	/* OpenCL variables */
	cl::Context context;
	std::vector <cl::Device> devices;
	cl::CommandQueue queue;
	cl::Program program;
	cl::Event ocltimer;

	OpenCL_initializations(context, devices, queue, DEBUG, CL_PROFILING, useGPU, openclDevice, openclPlatform);

	/* Building OpenCL program */
	std::string BuildOptions;
	std::string BuildLog;
	std::string FileName = CL_KNN;
	BuildOptions += "-cl-mad-enable -cl-no-signed-zeros -I . "; // Search for include files in current directory
	compile_OpenCL_code(BuildOptions, FileName, BuildLog, context, program, devices, openclDevice);

	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	cl_int err;

	/* Setting kernels */
	cl::Kernel parallel_distances = cl::Kernel(program, "parallel_distances_cached_worker", &err); check_err(err, "Kernel parallel_distances_cached_worker");

	/* Setup for parallel_distances */
	cl::Buffer dataBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*n*r, *data, &err); check_err(err, "Init dataBuffer");
	cl::Buffer distancesBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, (int)sizeof(cl_int)*n*q, NULL, &err); check_err(err, "Init indexBuffer");
	cl::Buffer queriesBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*q*r, *queries, &err); check_err(err, "Init queriesBuffer");

	err = parallel_distances.setArg(0, dataBuffer); check_err(err, "Set kernel parallel_distances_worker arg inputBuffer");
	err = parallel_distances.setArg(1, distancesBuffer); check_err(err, "Set kernel parallel_distances_worker arg distancesBuffer");
	err = parallel_distances.setArg(2, n); check_err(err, "Set kernel parallel_distances_worker arg n");
	err = parallel_distances.setArg(3, r); check_err(err, "Set kernel parallel_distances_worker arg r");
	err = parallel_distances.setArg(4, queriesBuffer); check_err(err, "Set kernel parallel_distances_worker arg queriesBuffer");
	err = parallel_distances.setArg(5, q); check_err(err, "Set kernel parallel_distances_worker arg q");
	err = parallel_distances.setArg(6, cl::__local(r*sizeof(cl_float))); check_err(err, "Set kernel parallel_distances_worker argument ldata (local)");
	// Arg 7 set later

#if TIMER
	cl_ulong start = 0, end = 0;
	cl_double total = 0.0;

	time_t ltime;
	time(&ltime);
	double timeElapsed = 0.0;
	Timer *t;
	t = new Timer();
#endif

	/* Do work: distances */
	int offset = 0;
	int workleft = q;
	int worksize; 
	do {
		worksize = min(workleft, workgroupSize);
		err = parallel_distances.setArg(7, offset); check_err(err, "Set kernel parallel_distances_worker arg offset"); // Used if q > wg
		err = queue.enqueueNDRangeKernel(parallel_distances, cl::NullRange, cl::NDRange(n*worksize), cl::NDRange(worksize), 0, &ocltimer); check_err(err, "Enqueue parallel_distances kernel");
		offset += worksize;
		workleft -= worksize;
#if CL_PROFILING
		err = queue.finish(); check_err(err, "Clear work queue");
		ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
		ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
		total += (cl_double)(end - start)*(cl_double)(1e-09);
#endif
	} while (workleft > 0);

	//err = queue.finish(); check_err(err, "Clear work queue");

	/* Download results from device */
	err = queue.enqueueReadBuffer(distancesBuffer, CL_TRUE, 0, sizeof(cl_float)*n*q, *distances, 0, 0); check_err(err, "Copy distancesBuffer from device");
#if TIMER
	timeElapsed = t->elapsed();
	std::cout << "OpenCL parallel_distances time: " << timeElapsed << "s" << std::endl;
	std::cout << "                      iterated: " << (q/workgroupSize) + 1 << " times" << std::endl;
	delete t;
#if CL_PROFILING
	std::cout << "OpenCL Device parallel_distances time: " << total << "s" << std::endl;
#endif
#endif
	/* Done with buffers */
	dataBuffer = cl::Buffer();
	distancesBuffer = cl::Buffer();
	queriesBuffer = cl::Buffer();
}

/* Interface to decide whether we want multiple or single object per wg */
void opencl_cached_distances(float *data[], float *distances[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	/* Determine number of workgroup objects */
	unsigned int wgobjects = 1; // If you're running with 1 partition, use the other method
	if (openclPlatform == NV_OPENCLPLATFORM) { // Nvidia, warp size 32, 48KB local mem
		if (q == 1 && r <= 12 && n >= 1024) wgobjects = 1024;
		else if (q <= 2 && r <= 24 && n >= 512) wgobjects = 512;
		else if (q <= 4 && r <= 48 && n >= 256) wgobjects = 256;
		else if (q <= 8 && r <= 96 && n >= 128) wgobjects = 128;
		else if (q <= 16 && r <= 192 && n >= 64) wgobjects = 64;
		else if (q <= 32 && r <= 384 && n >= 32) wgobjects = 32;
	} else if (openclPlatform == AMD_OPENCLPLATFORM) { // AMD, warp (wavefront) size 64, 32KB local mem
		if (!useGPU && q == 1 && r <= 8 && n >= 1024) wgobjects = 1024;
		else if (!useGPU && q <= 2 && r <= 16 && n >= 512) wgobjects = 512;
		else if (((!useGPU && q <= 4) || q <= 1) && r <= 32 && n >= 256) wgobjects = 256; // AMD GPUs max workgroup size 256
		else if (((!useGPU && q <= 8) || q <= 2) && r <= 64 && n >= 128) wgobjects = 128;
		else if (((!useGPU && q <= 16) || q <= 4) && r <= 128 && n >= 64) wgobjects = 64;
	}
	if (wgobjects > 1) {
		DBG(std::cout << "opencl_cached_distances: taking smallq path (work factor " << wgobjects << ")" << std::endl;);
		opencl_cached_smallq_distances(data, distances, n, r, k, queries, q, useGPU, openclDevice, openclPlatform, workgroupSize, workgroupCount, wgobjects);
	} else {
		DBG(std::cout << "opencl_cached_distances: taking multipass path (" << ceil((float)q/(float)workgroupSize) << " passes)" << std::endl;);
		opencl_cached_multipass_distances(data, distances, n, r, k, queries, q, useGPU, openclDevice, openclPlatform, workgroupSize, workgroupCount);
	}
}

/* KNN distance only w/ object caching only */
void opencl_threshold_smallq_distances(float *data[], float *distances[],  float *thresholds[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount, unsigned int workgroupObjects) {
	/* More init code borrowed from ranluxcltest.cpp */
	/* OpenCL variables */
	cl::Context context;
	std::vector <cl::Device> devices;
	cl::CommandQueue queue;
	cl::Program program;
	cl::Event ocltimer;

	OpenCL_initializations(context, devices, queue, DEBUG, CL_PROFILING, useGPU, openclDevice, openclPlatform);

	/* Building OpenCL program */
	std::string BuildOptions;
	std::string BuildLog;
	std::string FileName = CL_KNN;
	BuildOptions += "-cl-mad-enable -cl-no-signed-zeros -I . "; // Search for include files in current directory
	compile_OpenCL_code(BuildOptions, FileName, BuildLog, context, program, devices, openclDevice);

	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	cl_int err;

	unsigned int padding = (q*workgroupObjects) - ((n*q)%(q*workgroupObjects));

	/* Setting kernels */
	cl::Kernel parallel_distances = cl::Kernel(program, "parallel_distances_threshold_multi_worker", &err); check_err(err, "Kernel parallel_distances_threshold_multi_worker");

	/* Setup for parallel_distances */
	cl::Buffer dataBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*n*r, *data, &err); check_err(err, "Init dataBuffer");
	cl::Buffer thresholdsBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*q, *thresholds, &err); check_err(err, "Init thresholdsBuffer");
	cl::Buffer distancesBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, (int)sizeof(cl_int)*n*q, NULL, &err); check_err(err, "Init indexBuffer");
	cl::Buffer queriesBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*q*r, *queries, &err); check_err(err, "Init queriesBuffer");

	err = parallel_distances.setArg(0, dataBuffer); check_err(err, "Set kernel parallel_distances_threshold_multi_worker arg inputBuffer");
	err = parallel_distances.setArg(1, distancesBuffer); check_err(err, "Set kernel parallel_distances_threshold_multi_worker arg distancesBuffer");
	err = parallel_distances.setArg(2, thresholdsBuffer); check_err(err, "Set kernel parallel_distances_threshold_multi_worker arg thresholdsBuffer");
	err = parallel_distances.setArg(3, n); check_err(err, "Set kernel parallel_distances_threshold_multi_worker arg n");
	err = parallel_distances.setArg(4, r); check_err(err, "Set kernel parallel_distances_threshold_multi_worker arg r");
	err = parallel_distances.setArg(5, queriesBuffer); check_err(err, "Set kernel parallel_distances_threshold_multi_worker arg queriesBuffer");
	err = parallel_distances.setArg(6, q); check_err(err, "Set kernel parallel_distances_threshold_multi_worker arg q");
	err = parallel_distances.setArg(7, cl::__local(workgroupObjects*r*sizeof(cl_float))); check_err(err, "Set kernel parallel_distances_threshold_multi_worker argument ldata (local)");

#if TIMER
	time_t ltime;
	time(&ltime);
	double timeElapsed = 0.0;
	Timer *t;
	t = new Timer();
#endif
	/* Do work: distances */
	err = queue.enqueueNDRangeKernel(parallel_distances, cl::NullRange, cl::NDRange(n*q+padding), cl::NDRange(q*workgroupObjects), 0, &ocltimer); check_err(err, "Enqueue parallel_distances_threshold_multi_worker kernel");

	//err = queue.finish(); check_err(err, "Clear work queue");

	/* Download results from device */
	err = queue.enqueueReadBuffer(distancesBuffer, CL_TRUE, 0, sizeof(cl_float)*n*q, *distances, 0, 0); check_err(err, "Copy distancesBuffer from device");
#if TIMER
	timeElapsed = t->elapsed();
	std::cout << "OpenCL Host parallel_distances_threshold_multi_worker time:  " << timeElapsed << "s" << std::endl;
	delete t;
#if CL_PROFILING
	cl_ulong start = 0, end = 0;
	ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
	ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
	std::cout << "OpenCL Device parallel_distances_threshold_multi_worker time: " << (cl_double)(end - start)*(cl_double)(1e-09) << "s" << std::endl;
#endif
#endif
	/* Done with buffers */
	dataBuffer = cl::Buffer();
	distancesBuffer = cl::Buffer();
	queriesBuffer = cl::Buffer();
}

/* KNN distance only w/ object caching and optional multipass only */
void opencl_threshold_multipass_distances(float *data[], float *distances[], float *thresholds[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	/* More init code borrowed from ranluxcltest.cpp */
	/* OpenCL variables */
	cl::Context context;
	std::vector <cl::Device> devices;
	cl::CommandQueue queue;
	cl::Program program;
	cl::Event ocltimer;

	OpenCL_initializations(context, devices, queue, DEBUG, CL_PROFILING, useGPU, openclDevice, openclPlatform);

	/* Building OpenCL program */
	std::string BuildOptions;
	std::string BuildLog;
	std::string FileName = CL_KNN;
	BuildOptions += "-cl-mad-enable -cl-no-signed-zeros -I . "; // Search for include files in current directory
	compile_OpenCL_code(BuildOptions, FileName, BuildLog, context, program, devices, openclDevice);

	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	const int spaceLeft = (3*1024*0.95) - ((sizeof(float)*n*r) + (sizeof(float)*q*r) + (sizeof(float)*k*q))/(1024*1024);
	int qspaceUsed = (sizeof(float)*n*q)/(1024*1024);
	int qiters = 1;
	int qcount = q;
	while (qspaceUsed > spaceLeft || qcount > workgroupSize) {
		qcount /= 2;
		qiters *= 2;
		qspaceUsed /= 2;
	}

	std::cout << "Distance iterations: " << qiters << std::endl;

	cl_int err;

	/* Setting kernels */
	cl::Kernel parallel_distances = cl::Kernel(program, "parallel_distances_threshold_worker", &err); check_err(err, "Kernel parallel_distances_threshold_worker");

	/* Setup for parallel_distances */
	cl::Buffer dataBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, (int)sizeof(cl_float)*n*r, *data, &err); check_err(err, "Init dataBuffer");
	cl::Buffer thresholdsBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, (int)sizeof(cl_float)*qcount, NULL, &err); check_err(err, "Init thresholdsBuffer");
	cl::Buffer distancesBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, (int)sizeof(cl_int)*n*qcount, NULL, &err); check_err(err, "Init indexBuffer");
	cl::Buffer queriesBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, (int)sizeof(cl_float)*qcount*r, NULL, &err); check_err(err, "Init queriesBuffer");

	err = parallel_distances.setArg(0, dataBuffer); check_err(err, "Set kernel parallel_distances_threshold_worker arg inputBuffer");
	err = parallel_distances.setArg(1, distancesBuffer); check_err(err, "Set kernel parallel_distances_threshold_worker arg distancesBuffer");
	err = parallel_distances.setArg(2, thresholdsBuffer); check_err(err, "Set kernel parallel_distances_threshold_worker arg thresholdsBuffer");
	err = parallel_distances.setArg(3, n); check_err(err, "Set kernel parallel_distances_threshold_worker arg n");
	err = parallel_distances.setArg(4, r); check_err(err, "Set kernel parallel_distances_threshold_worker arg r");
	err = parallel_distances.setArg(5, queriesBuffer); check_err(err, "Set kernel parallel_distances_threshold_worker arg queriesBuffer");
	err = parallel_distances.setArg(6, qcount); check_err(err, "Set kernel parallel_distances_threshold_worker arg q");
	err = parallel_distances.setArg(7, cl::__local(r*sizeof(cl_float))); check_err(err, "Set kernel parallel_distances_threshold_worker argument ldata (local)");
	err = parallel_distances.setArg(8, 0); check_err(err, "Set kernel parallel_distances_threshold_worker arg offset");

#if TIMER
	cl_ulong start = 0, end = 0;
	cl_double total = 0.0;

	time_t ltime;
	time(&ltime);
	double timeElapsed = 0.0;
	Timer *t;
	t = new Timer();
#endif

	float *thresholdspart = (float *)malloc(sizeof(float)*qcount); check_alloc(thresholdspart);
	float *distancespart = (float *)malloc(sizeof(float)*n*qcount); check_alloc(distancespart);
	float *queriespart = (float *)malloc(sizeof(float)*qcount*r); check_alloc(queriespart);
	int offset = 0, iters = 0;

	/* Do work: distances */
	do {
		for (int i = offset; (i < offset+qcount) & (i < q); i++) {
			thresholdspart[i-offset] = (*thresholds)[i];

			for (int j = 0; j < r; j++) {
				queriespart[j*qcount+(i-offset)] = (*queries)[j*q+i];
			}
		}

	    queue.enqueueWriteBuffer(thresholdsBuffer, CL_TRUE, 0, (size_t)sizeof(cl_float)*qcount, thresholdspart); check_err(err, "Copy thresholdsBuffer to device");
		queue.enqueueWriteBuffer(distancesBuffer, CL_TRUE, 0, (size_t)sizeof(cl_float)*n*qcount, distancespart); check_err(err, "Copy distancesBuffer to device");
		queue.enqueueWriteBuffer(queriesBuffer, CL_TRUE, 0, (size_t)sizeof(cl_float)*qcount*r, queriespart); check_err(err, "Copy queriesBuffer to device");

		err = queue.enqueueNDRangeKernel(parallel_distances, cl::NullRange, cl::NDRange(n*qcount), cl::NDRange(qcount), 0, &ocltimer); check_err(err, "Enqueue parallel_distances_threshold_worker kernel");
		
		/* Download results from device */
		err = queue.enqueueReadBuffer(distancesBuffer, CL_TRUE, 0, sizeof(cl_float)*n*qcount, distancespart, 0, 0); check_err(err, "Copy distancesBuffer from device");
#if CL_PROFILING
		err = queue.finish(); check_err(err, "Clear work queue");
		ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
		ocltimer.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
		total += (cl_double)(end - start)*(cl_double)(1e-09);
#endif
		for (int64_t i = offset; (i < offset+qcount) & (i < q); i++) {
			for (int64_t j = 0; j < n; j++) {
				(*distances)[i*(int64_t)n+j] = distancespart[(i-offset)*(int64_t)n+j];
			}
		}

		iters++;
		offset += qcount;
	} while (iters < qiters);

#if TIMER
	timeElapsed = t->elapsed();
	std::cout << "OpenCL Host parallel_distances_threshold_worker time: " << timeElapsed << "s" << std::endl;
	std::cout << "                                            iterated: " << (q/workgroupSize) + 1 << " times" << std::endl;
	delete t;
#if CL_PROFILING
	std::cout << "OpenCL Device parallel_distances_threshold_worker time: " << total << "s" << std::endl;
#endif
#endif
	/* Done with buffers */
	dataBuffer = cl::Buffer();
	distancesBuffer = cl::Buffer();
	queriesBuffer = cl::Buffer();
}

/* Interface to decide whether we want multiple or single object per wg */
void opencl_threshold_distances(float *data[], float *distances[], float *thresholds[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	/* Determine number of workgroup objects */
	unsigned int wgobjects = 1; // If you're running with 1 partition, use the other method
	if (openclPlatform == NV_OPENCLPLATFORM) { // Nvidia, warp size 32, 48KB local mem
		if (q == 1 && r <= 12 && n >= 1024) wgobjects = 1024;
		else if (q <= 2 && r <= 24 && n >= 512) wgobjects = 512;
		else if (q <= 4 && r <= 48 && n >= 256) wgobjects = 256;
		else if (q <= 8 && r <= 96 && n >= 128) wgobjects = 128;
		else if (q <= 16 && r <= 192 && n >= 64) wgobjects = 64;
		else if (q <= 32 && r <= 384 && n >= 32) wgobjects = 32;
	} else if (openclPlatform == AMD_OPENCLPLATFORM) { // AMD, warp (wavefront) size 64, 32KB local mem
		if (!useGPU && q == 1 && r <= 8 && n >= 1024) wgobjects = 1024;
		else if (!useGPU && q <= 2 && r <= 16 && n >= 512) wgobjects = 512;
		else if (((!useGPU && q <= 4) || q <= 1) && r <= 32 && n >= 256) wgobjects = 256; // AMD GPUs max workgroup size 256
		else if (((!useGPU && q <= 8) || q <= 2) && r <= 64 && n >= 128) wgobjects = 128;
		else if (((!useGPU && q <= 16) || q <= 4) && r <= 128 && n >= 64) wgobjects = 64;
	}
	if (wgobjects > 1) {
		DBG(std::cout << "opencl_threshold_distances: taking smallq path (work factor " << wgobjects << ")" << std::endl;);
		opencl_threshold_smallq_distances(data, distances, thresholds, n, r, k, queries, q, useGPU, openclDevice, openclPlatform, workgroupSize, workgroupCount, wgobjects);
	} else {
		DBG(std::cout << "opencl_threshold_distances: taking multipass path (" << ceil((float)q/(float)workgroupSize) << " passes)" << std::endl;);
		opencl_threshold_multipass_distances(data, distances, thresholds, n, r, k, queries, q, useGPU, openclDevice, openclPlatform, workgroupSize, workgroupCount);
	}
}

/* KNN using BLAS */
void opencl_matrix_distances(float *data[], float *distances[], int n, int r, int k, float *queries[], int q, bool useGPU, unsigned int openclDevice, unsigned int openclPlatform, unsigned int workgroupSize, unsigned int workgroupCount) {
	/* More init code borrowed from ranluxcltest.cpp */
	/* OpenCL variables */
	cl::Context context;
	std::vector <cl::Device> devices;
	cl::CommandQueue queue;
	cl::Program program_sumsq, program_matrixd;
	cl::Event sumsquare1_event, sumsquare2_event, matrixdist_event;
	cl_event sgemm_dq_event;
	cl_int err;

	OpenCL_initializations(context, devices, queue, DEBUG, CL_PROFILING, useGPU, openclDevice, openclPlatform);

	/* Build OpenCL program */
	std::string BuildOptions;
	std::string BuildLog;
	std::string FileName = CL_KNN;
	BuildOptions += "-cl-fast-relaxed-math -I . "; // Search for include files in current directory

	compile_OpenCL_code(BuildOptions, FileName, BuildLog, context, program_sumsq, devices, openclDevice);
	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	compile_OpenCL_code(BuildOptions, FileName, BuildLog, context, program_matrixd, devices, openclDevice);
	DBG(std::cout << std::endl << "--Build log--" << std::endl << BuildLog << std::endl;);

	cl::Kernel parallel_sum_squares = cl::Kernel(program_sumsq, "parallel_sum_squares_worker", &err); check_err(err, "Kernel parallel_sum_squares_worker");
	cl::Kernel parallel_matrix_distance = cl::Kernel(program_matrixd, "parallel_matrix_distance_worker", &err); check_err(err, "Kernel parallel_matrix_distance_worker");

	/* Setup clBLAS */
	err = clblasSetup(); check_err(err, "Setup clBLAS");

	/* Setup for parallel_distances */
	cl::Buffer dataBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, (int)sizeof(cl_float)*n*r, NULL, &err); check_err(err, "Init dataBuffer");
	cl::Buffer queriesBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, (int)sizeof(cl_float)*q*r, NULL, &err); check_err(err, "Init queriesBuffer");
	cl::Buffer DvBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_float)*n, NULL, &err); check_err(err, "Init DvBuffer");
	cl::Buffer QvBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_float)*q, NULL, &err); check_err(err, "Init QvBuffer");
	cl::Buffer DBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, (int)sizeof(cl_float)*n*q, NULL, &err); check_err(err, "Init DBuffer");

	/* Send buffer to device */
    queue.enqueueWriteBuffer(dataBuffer, CL_TRUE, 0, (size_t)sizeof(cl_float)*n*r, *data); check_err(err, "Copy dataBuffer to device");
    queue.enqueueWriteBuffer(queriesBuffer, CL_TRUE, 0, (size_t)sizeof(cl_float)*q*r, *queries); check_err(err, "Copy queriesBuffer to device");
    queue.enqueueWriteBuffer(DvBuffer, CL_TRUE, 0, (size_t)sizeof(cl_float)*n, NULL); check_err(err, "Copy DvBuffer to device");
    queue.enqueueWriteBuffer(QvBuffer, CL_TRUE, 0, (size_t)sizeof(cl_float)*q, NULL); check_err(err, "Copy QvBuffer to device");
	queue.enqueueWriteBuffer(DBuffer, CL_TRUE, 0, (size_t)sizeof(cl_float)*n*q, NULL); check_err(err, "Copy DBuffer to device");

	err = parallel_sum_squares.setArg(0, dataBuffer); check_err(err, "Set kernel parallel_sum_squares_worker arg dataBuffer");
	err = parallel_sum_squares.setArg(1, DvBuffer); check_err(err, "Set kernel parallel_sum_squares_worker arg DvBuffer");
	err = parallel_sum_squares.setArg(2, n); check_err(err, "Set kernel parallel_sum_squares_worker arg n");
	err = parallel_sum_squares.setArg(3, r); check_err(err, "Set kernel parallel_sum_squares_worker arg r");
	err = parallel_sum_squares.setArg(4, cl::__local(r*sizeof(cl_float))); check_err(err, "Set kernel parallel_sum_squares_worker arg scratch (local)");

	err = queue.enqueueNDRangeKernel(parallel_sum_squares, cl::NullRange, cl::NDRange(n*r), cl::NDRange(r), 0, &sumsquare1_event); check_err(err, "Enqueue parallel_sum_squares_worker kernel (1)");
	err = queue.flush(); check_err(err, "Flush work queue parallel_sum_squares_worker");

	err = parallel_sum_squares.setArg(0, queriesBuffer); check_err(err, "Set kernel parallel_sum_squares_worker arg queriesBuffer");
	err = parallel_sum_squares.setArg(1, QvBuffer); check_err(err, "Set kernel parallel_sum_squares_worker arg QvBuffer");
	err = parallel_sum_squares.setArg(2, q); check_err(err, "Set kernel parallel_sum_squares_worker arg q");

	err = queue.enqueueNDRangeKernel(parallel_sum_squares, cl::NullRange, cl::NDRange(q*r), cl::NDRange(r), 0, &sumsquare2_event); check_err(err, "Enqueue parallel_sum_squares_worker kernel (2)");
	err = queue.finish(); check_err(err, "Clear work queue parallel_sum_squares_worker");

	/* SGEMM compute DQ */
	err = clblasSgemm(clblasRowMajor, clblasNoTrans, clblasTrans, n, q, r, 1.0f, dataBuffer(), 0, r, queriesBuffer(), 0, r, 0.0f, DBuffer(), 0, q, 1, &queue(), 0, NULL, &sgemm_dq_event); // n*q
	check_err(err, "Call clblasSgemm (DQ)");

	err = clWaitForEvents(1, &sgemm_dq_event);

	/* Done with buffers */
	dataBuffer = cl::Buffer();
	queriesBuffer = cl::Buffer();

	/* Done with clBLAS */
	clblasTeardown();

	err = parallel_matrix_distance.setArg(0, DBuffer); check_err(err, "Set kernel parallel_matrix_distance_worker arg DBuffer");
	err = parallel_matrix_distance.setArg(1, DvBuffer); check_err(err, "Set kernel parallel_matrix_distance_worker arg DvBuffer");
	err = parallel_matrix_distance.setArg(2, QvBuffer); check_err(err, "Set kernel parallel_matrix_distance_worker arg QvBuffer");
	err = parallel_matrix_distance.setArg(3, q); check_err(err, "Set kernel parallel_matrix_distance_worker arg dim");

	err = queue.enqueueNDRangeKernel(parallel_matrix_distance, cl::NullRange, cl::NDRange(n*q), cl::NDRange(q), 0, &matrixdist_event); check_err(err, "Enqueue parallel_matrix_distance_worker (1) kernel");

	err = queue.finish(); check_err(err, "Clear work queue parallel_matrix_distance_worker");

	cl_ulong start = 0, end = 0;
	sumsquare1_event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
	sumsquare1_event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
	std::cout << "OpenCL Device parallel_sum_squares_worker (1) time: " << (cl_double)(end - start)*(cl_double)(1e-09) << "s" << std::endl;
	sumsquare2_event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
	sumsquare2_event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
	std::cout << "OpenCL Device parallel_sum_squares_worker (2) time: " << (cl_double)(end - start)*(cl_double)(1e-09) << "s" << std::endl;
	clGetEventProfilingInfo(sgemm_dq_event, CL_PROFILING_COMMAND_START, sizeof(start), &start, NULL);
	clGetEventProfilingInfo(sgemm_dq_event, CL_PROFILING_COMMAND_END, sizeof(end), &end, NULL);
	std::cout << "OpenCL Device sgemm_dq_event time: " << (cl_double)(end - start)*(cl_double)(1e-09) << "s" << std::endl;
	matrixdist_event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
	matrixdist_event.getProfilingInfo(CL_PROFILING_COMMAND_END, &end);
	std::cout << "OpenCL Device parallel_matrix_distance_worker time: " << (cl_double)(end - start)*(cl_double)(1e-09) << "s" << std::endl;

	/* Download results from device */
	err = queue.enqueueReadBuffer(DBuffer, CL_TRUE, 0, sizeof(cl_float)*n*q, *distances, 0, 0); check_err(err, "Copy DBuffer from device");

	/* Done with buffers */
	DvBuffer = cl::Buffer();
	QvBuffer = cl::Buffer();
	DBuffer = cl::Buffer();
}
