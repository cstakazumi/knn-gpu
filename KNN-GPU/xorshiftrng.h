#ifndef XORSHIFTRNG_H
#define XORSHIFTRNG_H

unsigned int xorshift(unsigned int seed);
float xorshift_float(unsigned int seed);

#endif
