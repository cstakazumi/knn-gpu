#ifndef DATA_H
#define DATA_H

#include <string>
using std::string;

void synthetic_data_array(float* dataArray[], int length, int width, float outlierRate, float outlierOffset, int outlierDims);
void synthetic_data_array_clusters(float *data[], int length, int width, float outlierRate, float outlierOffset, int outlierDims, int outlierDistributions);
void delim_to_array(float* data[], string filename, int length, int width, char delimiter);
void csv_to_array(float* data[], string filename, int length, int width);
void bin_to_array(float* data[], string filename, int length, int width);
void normalize_data_array(float* data[], int length, int width);
void shuffle_array(float *data[], int length, int width, unsigned int seed);
void random_uncertainty(float *data[], const int length, const int width, float *stdev[], float minUncertainty, float maxUncertainty);
void switch_array_order(float *data[], float *dataR[], const int length, const int width);

#endif
